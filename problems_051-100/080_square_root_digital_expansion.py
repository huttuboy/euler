#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #80 Square root digital expansion
#
# Author: Tuomo Huttu
# Date:   28.12.2019 (syntax updated 21.5.2020)

# Right answer: 40886 (0.004s)



import time
from decimal import *
getcontext().prec = 105


def main():
    ret=0
    for i in range(1,101):
        aa=str(Decimal(i).sqrt()).replace(".","")
        if len(aa)>100:
            for j in range(100):
                ret+=int(aa[j])
    
    return ret



if __name__ == "__main__":
    start_time=time.time()
    print(main())
    print("time: "+str(round(time.time()-start_time,3))+"s")

