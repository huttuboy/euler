#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #58 Spiral primes
#
# Author: Tuomo Huttu
# Date:   21.4.2018 (syntax updated 21.5.2020)

# Right answer: 26241 (5.8s) (python2.7 3.4s)


import time
from primes import is_prime


def main():
    ispr=0 #prime count
    nopr=1 #no-prime count
    side=1
    i=1
    while True:
        side+=2
        for j in range(4):
            i+=(side-1)
            if is_prime(i):
                ispr+=1
            else:
                nopr+=1
        rate=1.0*ispr/(ispr+nopr)
        #print str(side)+" "+str(round(rate,2))
        #print str(ispr)+" "+str(nopr)
        if rate<0.1:
            break
    
    return side



if __name__ == "__main__":
    start_time=time.time()
    print(main())
    print("time: "+str(round(time.time()-start_time,3))+"s")

