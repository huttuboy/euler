#!/usr/bin/env python
# -*- coding: utf-8 -*-

#from triangular import *

from math import sqrt

def is_pentagonal(x):
    n=(1+sqrt(1+24*x))/6
    m=int(n)
    if x==m*(3*m-1)/2:
        return True
    return False

def is_hexagonal(x):
    n=(1+sqrt(1+8*x))/4
    m=int(n)
    if x==m*(2*m-1):
        return True
    return False

def is_square(x):
    n=sqrt(x)
    m=int(n)
    if x==m**2:
        return True
    return False

def is_heptagonal(x):
    n=(3+sqrt(9+40*x))/10
    m=int(n)
    if x==m*(5*m-3)/2:
        return True
    return False

def is_octagonal(x):
    n=(2+sqrt(4+12*x))/6
    m=int(n)
    if x==m*(3*m-2):
        return True
    return False



if __name__ == "__main__":
    print(is_square(16))
    print(is_square(17))
    print(is_heptagonal(34))
    print(is_heptagonal(35))
    print(is_octagonal(40))
    print(is_octagonal(41))

