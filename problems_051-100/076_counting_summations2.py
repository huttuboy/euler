#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #76 Counting summations
#
# Author: Tuomo Huttu
# Date:   5.8.2019 (syntax updated 21.5.2020)

# Right answer: 190569291 (0.006s)


import time


def main(x):
    combos=[]
    for i in range(x+1):
        combos.append([])
    combos[1]=[0,1]
    combos[2]=[0,1]
    combos[3]=[0,1,1]

    for i in range(4,x+1):
        cc=splitter(i)
        cc2=[]
        for j in range(i):
            cc2.append(0)
        cc2[0]=0

        for c in cc:
            if c[1]==1:
                cc2[c[0]]+=1
            elif c[0]>=c[1]:
                cc2[c[0]]+=sum(combos[c[1]])
                cc2[c[0]]+=1
            elif c[0]<c[1]:
                cc2[c[0]]+=sum(combos[c[1]][:c[0]+1])
        combos[i]=cc2

    return sum(combos[x])


def splitter(a):
    ret=[]
    for i in range(1,a):
        ret.append([a-i,i])
    return ret



if __name__ == "__main__":
    start_time=time.time()
    #print str(main(5))  #6
    #print str(main(6))  #
    #print str(main(12)) #76
    #print str(main(18)) #384
    print(main(100)) #190569291
    print("time: "+str(round(time.time()-start_time,3))+"s")

