#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #73 Counting fractions in a range
#
# Author: Tuomo Huttu
# Date:   4.8.2019 (syntax updated 21.5.2020)

# Right answer: 7295372 (4.1s)
# (https://en.wikipedia.org/wiki/Farey_sequence)



import time


def main():
    #n/d
    limit=12000
    lkm=0
    #start a/b=1/3 , a/b c/d adjacent (vierekkäisiä) if and only if bc – ad = 1
    # => c/d=4000/11999
    a,b,c,d=1,3,4000,11999
    while c!=1 and d!=2:
        lkm+=1
        #if lkm%1000==0:
        #    print str(lkm)
        k=int((limit+b)/d)
        a,b,c,d=c,d,(k*c-a),(k*d-b)
    
    return lkm
    

if __name__ == "__main__":
    start_time=time.time()
    print(main())
    print("time: "+str(round(time.time()-start_time,3))+"s")

