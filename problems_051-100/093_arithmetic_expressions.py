#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #93 Arithmetic expressions
#
# Author: Tuomo Huttu
# Date:   2.1.2020 (syntax updated 22.5.2020)

# Right answer: 1258 (19s) (python2.7 12.5s)


import time
from itertools import permutations


oper_perms=0


def main():
    global oper_perms
    oper_perms=[]#list(permutations('+-*/', 3))
    for a in "+-*/":
        for b in "+-*/":
            for c in "+-*/":
                oper_perms.append([a,b,c])
    #return get_consecutive(1,2,3,4)

    pisin=0
    pisin_nums=0
    for a in range(1,7):
        for b in range(a+1,8):
            for c in range(b+1,9):
                for d in range(c+1,10):
                    pit=get_consecutive(a,b,c,d)
                    if pit>pisin:
                        pisin=pit
                        pisin_nums=str(a)+str(b)+str(c)+str(d)
                        #print pisin_nums+"  "+str(pit)
    return pisin_nums


def get_consecutive(a,b,c,d):
    global oper_perms
    perms=list(permutations([float(a),float(b),float(c),float(d)]))
    
    ev_list=[]
    for p in perms:
        for c in oper_perms:
            #abcd
            eval_help(str(p[0])+c[0]+str(p[1])+c[1]+str(p[2])+c[2]+str(p[3]),ev_list)
            
            #a(bc)d
            eval_help(str(p[0])+c[0]+"("+str(p[1])+c[1]+str(p[2])+")"+c[2]+str(p[3]),ev_list)
            
            #ab(cd)
            eval_help(str(p[0])+c[0]+str(p[1])+c[1]+"("+str(p[2])+c[2]+str(p[3])+")",ev_list)
            
            #a(bcd)
            eval_help(str(p[0])+c[0]+"("+str(p[1])+c[1]+str(p[2])+c[2]+str(p[3])+")",ev_list)
            
            #(ab)(cd)
            eval_help("("+str(p[0])+c[0]+str(p[1])+")"+c[1]+"("+str(p[2])+c[2]+str(p[3])+")",ev_list)
            
    ev_list.sort()
    #print ev_list
    #print str(len(ev_list))

    i=0
    while i<len(ev_list):
        i+=1
        if i not in ev_list:
            return i-1
    return i

def eval_help(evl,ev_list):
    try:
        ev=eval(evl)
        if ev>=1 and ev==int(ev) and int(ev) not in ev_list:
            ev_list.append(int(ev))
        return
    except:
        return



if __name__ == "__main__":
    start_time=time.time()
    print(main())
    print("time: "+str(round(time.time()-start_time,3))+"s")

