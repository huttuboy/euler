#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #97 Large non-Mersenne prime
#
# Author: Tuomo Huttu
# Date:   5.1.2020 (syntax updated 22.5.2020)

# Right answer: 8739992577 (0s)


import time


def main():
    ret=28433*pow(2,7830457,10000000000)+1
    aa=str(ret)
    return aa[len(aa)-10:]



if __name__ == "__main__":
    start_time=time.time()
    print(main())
    print("time: "+str(round(time.time()-start_time,3))+"s")

