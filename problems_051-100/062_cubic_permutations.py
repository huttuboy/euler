#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #62 Cubic permutations
#
# Author: Tuomo Huttu
# Date:   22.4.2018 (syntax updated 21.5.2020)

# Right answer: 127035954683 (6.5s) (python2.7 4.9s)


import time
from is_permutations import *


def main():
    for p in range(12,14):
        #print str(p)
        cubes=[]
        n=int((10**(p-1))**0.333333)
        while True:
            n+=1
            aa=n**3
            if aa>=10**p:
                break
            elif aa>=10**(p-1):
                cubes.append(aa)
        
        le=len(cubes)
        for a1 in range(le-4):
            for a2 in range(a1+1,le-3):
                if not is_permutations(cubes[a1],cubes[a2]):
                    continue
                for a3 in range(a2+1,le-2):
                    if not is_permutations(cubes[a1],cubes[a3]):
                        continue
                    elif not is_permutations(cubes[a2],cubes[a3]):
                        continue
                    for a4 in range(a3+1,le-1):
                        if not is_permutations(cubes[a1],cubes[a4]):
                            continue
                        elif not is_permutations(cubes[a2],cubes[a4]):
                            continue
                        elif not is_permutations(cubes[a3],cubes[a4]):
                            continue
                        for a5 in range(a4+1,le):
                            if not is_permutations(cubes[a1],cubes[a5]):
                                continue
                            elif not is_permutations(cubes[a2],cubes[a5]):
                                continue
                            elif not is_permutations(cubes[a3],cubes[a5]):
                                continue
                            elif not is_permutations(cubes[a4],cubes[a5]):
                                continue
                            print([cubes[a1],cubes[a2],cubes[a3],cubes[a4],cubes[a5]])
                            return cubes[a1]
    return 0



if __name__ == "__main__":
    start_time=time.time()
    print(main())
    print("time: "+str(round(time.time()-start_time,3))+"s")

