#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #74 Digit factorial chains
#
# Author: Tuomo Huttu
# Date:   4.8.2019 (syntax updated 21.5.2020)

# Right answer: 402 (30s)



import time
#from factorial import *
from math import factorial

fact_sums=[]
limit=2000000


def main(x):
    global fact_sums,limit

    for i in range(limit+1):
        fact_sums.append(0)
    
    suurin=0
    lkm60=0
    for a in range(1,x):
        if a%10000==0:
            print(a)
        lkm=1
        terms=[a]
        b=a
        while True:
            b=factorial_sum(b)
            if b>suurin:
                suurin=b
                print(suurin)
            if b not in terms:
                lkm+=1
                terms.append(b)
            else:
                break
        #print str(a)+"  "+str(lkm)
        if lkm==60:
            lkm60+=1

    return lkm60


def factorial_sum(x):
    global fact_sums,limit

    if x<limit and fact_sums[x]:
        return fact_sums[x]
    else:
        ret=0
        for c in str(x):
            ret+=factorial(int(c))
        if x<limit:
            fact_sums[x]=ret
        return ret



if __name__ == "__main__":
    start_time=time.time()
    print(main(1000000))
    print("time: "+str(round(time.time()-start_time,3))+"s")

