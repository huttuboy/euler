#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #63 Powerful digit counts
#
# Author: Tuomo Huttu
# Date:   22.4.2018 (syntax updated 21.5.2020)

# Right answer: 49 (0.001s)


import time


def main():
    ret=[]
    for p in range(1,100):
        a=0
        while True:
            a+=1
            aa=a**p
            if len(str(aa))==p and aa not in ret:
                ret.append(aa)
            elif len(str(aa))>p:
                break
    
    return len(ret)



if __name__ == "__main__":
    start_time=time.time()
    print(main())
    print("time: "+str(round(time.time()-start_time,3))+"s")

