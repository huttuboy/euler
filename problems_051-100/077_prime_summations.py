#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #77 Prime summations
#
# Author: Tuomo Huttu
# Date:   6.8.2019 (syntax updated 21.5.2020)

# Right answer: 71 (0.003s)



import time
from primes import *


def main(x):
    combos=[]
    for i in range(x+1):
        combos.append([])

    for i in range(4,x+1):
        cc=splitter(i)
        cc2=[]
        for j in range(i):
            cc2.append(0)
        cc2[0]=0

        for c in cc:
            if c[1] in [2,3]:
                cc2[c[0]]+=1
            elif c[0]>=c[1]:
                cc2[c[0]]+=sum(combos[c[1]])
                cc2[c[0]]+=1
            elif c[0]<c[1]:
                cc2[c[0]]+=sum(combos[c[1]][:c[0]+1])
        combos[i]=cc2
        #print cc2
        if sum(cc2)>5000:
            return i

    #for i in range(4,x+1):
    #    print str(i)+"  "+str(sum(combos[i]))
    return sum(combos[x])


def splitter(a):
    if a%2==0:
        pr=get_primelist(2,a-2)
    else:
        pr=get_primelist(3,a-2)
    ret=[]
    le=len(pr)
    for i in range(le):
        num=pr[le-i-1]
        if a-num>1:
            ret.append([num,a-num])
    return ret



if __name__ == "__main__":
    start_time=time.time()
    print(main(100))
    print("time: "+str(round(time.time()-start_time,3))+"s")

