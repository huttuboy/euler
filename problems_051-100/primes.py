#!/usr/bin/env python
# -*- coding: utf-8 -*-


prime_list=[2,3]


def primes(x):
    global prime_list
    
    last=prime_list[len(prime_list)-1]
    ret=[]
    x2=x
    i=0
    while True:
        if i>=len(prime_list):
            last=new_prime(last)
            prime_list.append(last)
        if x2%prime_list[i]==0:
            ret.append(prime_list[i])
            x2/=prime_list[i]
        else:
            i+=1
        if x2==1:
            break
    return ret


def primes_non_multi(x):
    distinct_primes(x)


def distinct_primes(x):
    pr=primes(x)
    ret=[]
    last=0
    for p in pr:
        if p>last:
            last=p
            ret.append(p)
    return ret


def new_prime(last):
    new=last
    while True:
        new+=2
        if is_prime(new):
            return new


def is_prime(x):
    if x<=1:
        return False
    elif x<4:
        return True
    elif x%2==0 or x%3==0:
        return False
    elif x>9:
        r=int(x**0.5)
        f=5
        while f<=r:
            if x%f==0 or x%(f+2)==0:
                return False
            f+=6
    return True


def get_primelist(lowerlimit=0,upperlimit=100):
    if lowerlimit<=2:
        ret=[2,3]
        last=3
    elif lowerlimit==3:
        ret=[3]
        last=3
    else:
        ret=[]
        if lowerlimit%2:
            last=lowerlimit-2
        else:
            last=lowerlimit-1

    while last+2<=upperlimit:
        last+=2
        if is_prime(last):
            ret.append(last)
    return ret


def get_n_primes(n):
    global prime_list
    
    le=len(prime_list)
    last=prime_list[le-1]
    while le<n:
        new=new_prime(last)
        prime_list.append(new)
        last=new
        le+=1

    return prime_list[0:n]



if __name__ == "__main__":
    print(primes(220))
    print(get_primelist(16,37))
    print(primes(134043))

