#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #59 XOR decryption
#
# Author: Tuomo Huttu
# Date:   21.4.2018 (syntax updated 21.5.2020)

# Right answer: 107359 (4.935s)



import time
from cipher_list import *


def main():
    abc="abcdefghijklmnopqrstuvwxyz"
    ci=get_cipher()
    ci=ci.split(",")

    ret=0
    ok=False
    for a1 in range(len(abc)):
        for a2 in range(len(abc)):
            for a3 in range(len(abc)):
                key=[ltr_to_ascii(abc[a1]),ltr_to_ascii(abc[a2]),ltr_to_ascii(abc[a3])]
                decrypted=msg_decrypt(ci,key)
                de=decrypted_to_str(decrypted)
                if "{" in de or "}" in de or "<" in de or ">" in de or "#" in de:
                    pass
                elif "The" in de:
                    #print(key)
                    #print(de)
                    ret=sum(decrypted)
                    ok=True
                    break
            if ok:
                break
        if ok:
            break
    return ret


def msg_decrypt(msg,key):
    ret=[]
    i=0
    for j in range(len(msg)):
        ret.append(xor(int(msg[j]),key[i]))
        i+=1
        if i==len(key):
            i=0
    return ret


def decrypted_to_str(msg):
    ret=""
    for m in msg:
        ret+=ascii_to_ltr(m)
    return ret


def xor(a,b):
    return a^b


def ltr_to_ascii(a):
    return ord(a)


def ascii_to_ltr(a):
    return chr(a)



if __name__ == "__main__":
    start_time=time.time()
    print(main())
    print("time: "+str(round(time.time()-start_time,3))+"s")

