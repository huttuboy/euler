#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #57 Square root convergents
#
# Author: Tuomo Huttu
# Date:   21.4.2018 (syntax updated 21.5.2020)

# Right answer: 153 (12s) (python2.7 83s)


import time,sys
from fractions import Fraction


def main():
    sys.setrecursionlimit(1020)
    ret=0
    for i in range(1,1001):
        if i%10==0:
            print(i)
        a=Fraction(1+sqr_frac(i))
        a=str(a)
        a=a.split("/")
        if len(a[0])>len(a[1]):
            ret+=1
    
    return ret



def sqr_frac(i):
    if i>1:
        return Fraction(1,2+sqr_frac(i-1))
    else:
        return Fraction(1,2)


if __name__ == "__main__":
    start_time=time.time()
    print(main())
    print("time: "+str(round(time.time()-start_time,3))+"s")

