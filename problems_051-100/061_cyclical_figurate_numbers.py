#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #61 Cyclical figurate numbers
#
# Author: Tuomo Huttu
# Date:   21.4.2018 (syntax updated 21.5.2020)

# Right answer: 28684 (0.58s) (python2.7 0.51s)


import time
from itertools import permutations
from triangular import *


def main():
    tri_list=[]
    n=1
    while True:
        n+=1
        #tri=n*(n+1)/2
        tri=n*(n+1)//2
        if tri>9999:
            break
        elif tri>999:
            if int(str(tri)[2:])>9:
                tri_list.append(tri)
    
    perm=list(permutations(range(1,6)))

    for p in perm:
        for t in tri_list:
            bb=p[0]
            a1_list=[]
            for i in range(10,100):
                aa=int(str(t)[2:]+str(i))
                if is_gonal(aa,bb):
                    a1_list.append(aa)
            for a1 in a1_list:
                bb=p[1]
                a2_list=[]
                for i in range(10,100):
                    aa=int(str(a1)[2:]+str(i))
                    if is_gonal(aa,bb):
                        a2_list.append(aa)
                for a2 in a2_list:
                    bb=p[2]
                    a3_list=[]
                    for i in range(10,100):
                        aa=int(str(a2)[2:]+str(i))
                        if is_gonal(aa,bb):
                            a3_list.append(aa)
                    for a3 in a3_list:
                        bb=p[3]
                        a4_list=[]
                        for i in range(10,100):
                            aa=int(str(a3)[2:]+str(i))
                            if is_gonal(aa,bb):
                                a4_list.append(aa)
                        for a4 in a4_list:
                            bb=p[4]
                            a5_list=[]
                            for i in range(10,100):
                                aa=int(str(a4)[2:]+str(i))
                                if is_gonal(aa,bb):
                                    a5_list.append(aa)
                            for a5 in a5_list:
                                #print str(t)+" "+str(a1)+" "+str(a2)+" "+str(a3)+" "+str(a4)+" "+str(a5)
                                if str(a5)[2:]==str(t)[0:2]:
                                    return t+a1+a2+a3+a4+a5
    return


def is_gonal(aa,bb):
    if bb==1 and is_square(aa):
        return True
    elif bb==2 and is_pentagonal(aa):
        return True
    elif bb==3 and is_hexagonal(aa):
        return True
    elif bb==4 and is_heptagonal(aa):
        return True
    elif bb==5 and is_octagonal(aa):
        return True
    return False



if __name__ == "__main__":
    start_time=time.time()
    print(main())
    print("time: "+str(round(time.time()-start_time,3))+"s")

