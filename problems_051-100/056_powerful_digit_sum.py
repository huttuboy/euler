#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #56 Powerful digit sum
#
# Author: Tuomo Huttu
# Date:   20.4.2018 (syntax updated 21.5.2020)

# Right answer: 972 (0.22s)


import time


def main():
    ret=0
    for a in range(100):
        for b in range(100):
            ds=digital_sum(a**b)
            if ds>ret:
                ret=ds
    
    return ret


def digital_sum(x):
    a=str(x)
    ret=0
    for i in range(len(a)):
        ret+=int(a[i])
    return ret



if __name__ == "__main__":
    start_time=time.time()
    print(main())
    print("time: "+str(round(time.time()-start_time,3))+"s")

