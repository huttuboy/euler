#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #79 Passcode derivation
#
# Author: Tuomo Huttu
# Date:   28.12.2019 (syntax updated 21.5.2020)

# Right answer: 73162890 (0s)



import time


def main():
    L=read_file("p079_keylog.txt")
    
    # 1 0 2 3 6 7 8 9
    
    aiemmat=[[],[],[],[],[],[],[],[],[],[]] #befores
    jalemmat=[[],[],[],[],[],[],[],[],[],[]] #afters

    for li in L:
        a1=int(li[0])
        a2=int(li[1])
        a3=int(li[2])

        if a2 not in jalemmat[a1]:
            jalemmat[a1].append(a2)
        if a3 not in jalemmat[a1]:
            jalemmat[a1].append(a3)
        jalemmat[a1].sort()

        if a1 not in aiemmat[a2]:
            aiemmat[a2].append(a1)
        if a3 not in jalemmat[a2]:
            jalemmat[a2].append(a3)
        aiemmat[a2].sort()
        jalemmat[a2].sort()

        if a1 not in aiemmat[a3]:
            aiemmat[a3].append(a1)
        if a2 not in aiemmat[a3]:
            aiemmat[a3].append(a2)
        aiemmat[a3].sort()
    
    #print "aiemmat:"
    #for i in range(10):
    #    print str(i)+" "+str(aiemmat[i])
    #print "jälemmät:"
    #for i in range(10):
    #    print str(i)+" "+str(jalemmat[i])
    
    ret=""
    for i in range(8):
        for j in range(10):
            if len(aiemmat[j])==i and len(jalemmat[j])==7-i:
                ret+=str(j)
                break
    
    return int(ret)





def read_file(file):
    L=[]
    f = open(file, "r")
    for line in f:
        l2=int(line)
        if l2 not in L:
            L.append(l2)
    f.close()
    L.sort()

    L2=[]
    for li in L:
        L2.append(str(li))
    
    return L2


if __name__ == "__main__":
    start_time=time.time()
    print(main())
    print("time: "+str(round(time.time()-start_time,3))+"s")

