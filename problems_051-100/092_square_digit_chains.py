#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #92 Square digit chains
#
# Author: Tuomo Huttu
# Date:   2.1.2020 (syntax updated 21.5.2020)

# Right answer: 8581146 (40s)


import time
squares=[0]*568 #7*9^2=567
squares[1]=1
squares[89]=89


def main(x):
    ret=0
    for i in range(1,x):
        if i%100000==0:
            print(i)
        if sq_digits(i)==89:
            ret+=1
    return ret


def sq_digits(num):
    global squares
    if num<568 and squares[num]>0:
        return squares[num]
    else:
        ret=0
        for n in str(num):
            ret+=int(n)**2
        if ret==89 or ret==1:
            return ret
        else:
            if squares[ret]>0:
                return squares[ret]
            else:
                a=sq_digits(ret)
                squares[ret]=a
                return a



if __name__ == "__main__":
    start_time=time.time()
    print(main(10000000))
    print("time: "+str(round(time.time()-start_time,3))+"s")

