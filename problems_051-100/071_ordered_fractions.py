#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #71 Ordered fractions
#
# Author: Tuomo Huttu
# Date:   3.8.2019 (syntax updated 21.5.2020)

# Right answer: 428570 (0.012s)


# HCF == highest common factor (gcd)


import time
from fractions import gcd,Fraction


def main():
    #n/d
    last=0
    for d in range(999000,1000001):
        #if d%1000==0:
        #    print str(d)
        for n in range(int(d/7.*3),int(d/7.*3)+1):
            if gcd(d,n)==1:
                if Fraction(n,d)<Fraction(3,7) and Fraction(n,d)>last:
                    last=Fraction(n,d)
    return last
    

if __name__ == "__main__":
    start_time=time.time()
    print(main())
    print("time: "+str(round(time.time()-start_time,3))+"s")

