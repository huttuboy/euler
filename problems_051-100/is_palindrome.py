#!/usr/bin/env python
# -*- coding: utf-8 -*-



def is_palindrome(x):
    a=str(x)
    if a==a[::-1]:
        return True
    return False



if __name__ == "__main__":
    print(is_palindrome("121"))
    print(is_palindrome(121))

