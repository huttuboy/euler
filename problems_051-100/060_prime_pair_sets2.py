#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #60 Prime pair sets
#
# Author: Tuomo Huttu
# Date:   15.1.2020 (syntax updated 21.5.2020)

# Right answer: 26033 (6.3s) (python2.7 4.6s)


import time
from primes import *
from powerset import powerset2


def main():
    prime_list=get_n_primes(1100)
    
    le=len(prime_list)
    for i in range(1,le-4):
        #print str(i)
        a1=prime_list[i]
        for i2 in range(i+1,le-3):
            a2=prime_list[i2]
            if not is_concatenate(a1,a2):
                continue
            for i3 in range(i2+1,le-2):
                a3=prime_list[i3]
                if not is_concatenate(a1,a3):
                    continue
                elif not is_concatenate(a2,a3):
                    continue
                for i4 in range(i3+1,le-1):
                    a4=prime_list[i4]
                    if not is_concatenate(a1,a4):
                        continue
                    elif not is_concatenate(a2,a4):
                        continue
                    elif not is_concatenate(a3,a4):
                        continue
                    for i5 in range(i4+1,le):
                        a5=prime_list[i5]
                        if not is_concatenate(a1,a5):
                            continue
                        elif not is_concatenate(a2,a5):
                            continue
                        elif not is_concatenate(a3,a5):
                            continue
                        elif not is_concatenate(a4,a5):
                            continue
                        #print str(i5)
                        return sum([a1,a2,a3,a4,a5])
    return


def is_concatenate(a,b):
    if not is_prime(int(str(a)+str(b))):
        return False
    if not is_prime(int(str(b)+str(a))):
        return False
    return True



if __name__ == "__main__":
    start_time=time.time()
    print(main())
    print("time: "+str(round(time.time()-start_time,3))+"s")

