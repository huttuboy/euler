#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #94 Almost equilateral triangles
#
# Author: Tuomo Huttu
# Date:   3.1.2020 (syntax updated 22.5.2020)

# Right answer: 518408346 (>5min) (python2.7 >4min)


import time,math
from decimal import *
getcontext().prec = 30


def main():
    total=0

    a=3
    while a<=333333333: #333333333
        if a%9999999==0:
            print(str(a))
        
        #A=bh/2, h=sqrt(a^2-1/4*b^2)
        b=a+1
        if b*b%4==0:
            h=math.sqrt(a*a-.25*b*b)
            #if h==long(h):
            if h==int(h):
                h=Decimal(a*a-Decimal(0.25*b*b)).sqrt()
                #if h==long(h):# and long(a*a-.25*b*b)==long(h)*long(h):
                if h==int(h):
                    total+=3*a+1
                    print(str(a)+" +1")
        
        b=a-1
        if b*b%4==0:
            h=math.sqrt(a*a-.25*b*b)
            #if h==long(h):
            if h==int(h):
                h=Decimal(a*a-Decimal(0.25*b*b)).sqrt()
                #if h==long(h):# and long(a*a-.25*b*b)==long(h)*long(h):
                if h==int(h):
                    total+=3*a-1
                    print(str(a)+" -1")
        a+=2
    
    return total



if __name__ == "__main__":
    start_time=time.time()
    print(main())
    print("time: "+str(round(time.time()-start_time,3))+"s")

