#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #96 Su Doku
#
# Author: Tuomo Huttu
# Date:   4.1.2020 (syntax updated 22.5.2020)

# Right answer: 24702 (13.6s) (python2.7 11.3s)


import time,random


def main():
    sudoku=read_file("p096_sudoku.txt")
    ret=0
    ii=0
    
    for s in sudoku:
        ii+=1
        choices_chart=[]
        for k in range(9):
            choices_chart.append([[],[],[],[],[],[],[],[],[]])
        
        last=""
        while is_zeros(s):
            new=sudo_to_str(s)
            if last!=new:
                last=new
                for r in range(9):      #rows
                    for c in range(9):  #cols
                        if s[r][c]==0:
                            choices=get_choices(s,r,c)
                            if len(choices)==1:
                                s[r][c]=choices[0]
                                choices_chart[r][c]=[]
                            else:
                                choices_chart[r][c]=choices
            else:
                #rows
                for r in range(9):
                    num_counts=[0,0,0,0,0,0,0,0,0,0]
                    for c in range(9):
                        for num in range(1,10):
                            if num in choices_chart[r][c]:
                                num_counts[num]+=1
                    if 1 in num_counts:
                        num=num_counts.index(1)
                        for c in range(9):
                            if num in choices_chart[r][c]:
                                s[r][c]=num
                                choices_chart[r][c]=[]
                                last=""
                                break
                    if not last:
                        break
                if not last:
                    continue
                
                #cols
                for c in range(9):
                    num_counts=[0,0,0,0,0,0,0,0,0,0]
                    for r in range(9):
                        for num in range(1,10):
                            if num in choices_chart[r][c]:
                                num_counts[num]+=1
                    if 1 in num_counts:
                        num=num_counts.index(1)
                        for r in range(9):
                            if num in choices_chart[r][c]:
                                s[r][c]=num
                                choices_chart[r][c]=[]
                                last=""
                                break
                    if not last:
                        break
                if not last:
                    continue

                #cells
                for cell in [[0,0],[3,0],[6,0],[0,3],[3,3],[6,3],[0,6],[3,6],[6,6]]:
                    num_counts=[0,0,0,0,0,0,0,0,0,0]
                    for r in range(cell[0],cell[0]+3):
                        for c in range(cell[1],cell[1]+3):
                            for num in range(1,10):
                                if num in choices_chart[r][c]:
                                    num_counts[num]+=1
                    if 1 in num_counts:
                        num=num_counts.index(1)
                        for r in range(cell[0],cell[0]+3):
                            for c in range(cell[1],cell[1]+3):
                                if num in choices_chart[r][c]:
                                    s[r][c]=num
                                    choices_chart[r][c]=[]
                                    last=""
                                    break
                            if not last:
                                break
                    if not last:
                        break
                if not last:
                    continue
            s=brute(s,0,0)

        #print str(ii)+" ok"
        ret+=int(str(s[0][0])+str(s[0][1])+str(s[0][2]))
    return ret


def brute(sudoku,r,c):
    if sudoku[r][c]>0:
        if c<8:
            return brute(sudoku,r,c+1)
        elif r<8:
            return brute(sudoku,r+1,0)
        else:
            return sudoku
    else:
        choices=get_choices(sudoku,r,c)
        if len(choices)==0:
            return 0
        
        s2=copy_sudoku(sudoku)
        if len(choices)==1:
            s2[r][c]=choices[0]
            if c<8:
                return brute(s2,r,c+1)
            elif r<8:
                return brute(s2,r+1,0)
            else:
                return s2
        else: #len(choices)>1
            for ch in choices:
                s2[r][c]=ch
                if c<8:
                    r_next=r
                    c_next=c+1
                elif r<8:
                    r_next=r+1
                    c_next=0
                
                sol=brute(s2,r_next,c_next)
                if sol:
                    return sol
            return 0


def sudo_to_str(sudoku):
    ret=""
    for r in sudoku:
        ret+=str(r)
    return ret


def copy_sudoku(sudoku):
    ret=[]
    for r in sudoku:
        ret.append(list(r))
    return ret


def get_choices(sudoku,r,c):
    row_nums=sudoku[r]
    col_nums=[]
    cell_nums=[]
    for i in range(9):
        row=sudoku[i]
        col_nums.append(row[c])
        if i in range(r-r%3,r-r%3+3):
            cell_nums+=row[c-c%3:c-c%3+3]
    ret=[]
    nums=row_nums+col_nums+cell_nums
    for i in range(1,10):
        if i not in nums:
            ret.append(i)
    
    return ret


def is_zeros(sudoku):
    for r in sudoku:
        if 0 in r:
            return True
    return False


def read_file(file):
    ret=[]
    f = open(file, "r")
    sudo=[]
    for line in f:
        if len(line)>=9:
            nums=[]
            for n in line[0:9]:
                nums.append(int(n))
            sudo.append(nums)
            if len(sudo)==9:
                ret.append(sudo)
                sudo=[]
    f.close()
    
    return ret



if __name__ == "__main__":
    start_time=time.time()
    print(main())
    print("time: "+str(round(time.time()-start_time,3))+"s")

