#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #57 Square root convergents
#
# Author: Tuomo Huttu
# Date:   21.4.2018 (syntax updated 21.5.2020)

# Right answer: 153 (0.004s)


import time


def main():
    a=3
    b=2
    ret=0
    for i in range(2,1001):
        uusi_a=a+2*b
        uusi_b=a+b
        a=uusi_a
        b=uusi_b
        #print str(uusi_a)+"/"+str(uusi_b)
        if len(str(uusi_a))>len(str(uusi_b)):
            ret+=1
    
    return ret


if __name__ == "__main__":
    start_time=time.time()
    print(main())
    print("time: "+str(round(time.time()-start_time,3))+"s")

