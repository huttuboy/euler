#!/usr/bin/env python
# -*- coding: utf-8 -*-



def is_permutations(a,b):
    a2=str(a)
    b2=str(b)
    if len(a2)!=len(b2):
        return False
    for i in range(len(a2)):
        j=b2.find(a2[i])
        if j<0:
            return False
        b2=b2[:j]+b2[(j+1):]
    return True


def is_permutations2(a,b): #voi olla hitaampi (esim #62)
    a2=str(a)
    b2=str(b)
    if len(a2)!=len(b2):
        return False
    
    a2=list(a2)
    a2.sort()
    b2=list(b2)
    b2.sort()
    if a2!=b2:
        return False
    return True



if __name__ == "__main__":
    print(is_permutations("121",211))
    print(is_permutations(1234,3124))
    print(is_permutations2("121",211))
    print(is_permutations2(1234,3124))
    print(is_permutations(1234,324))
    print(is_permutations(10648000,40001688))
    print(is_permutations2(1234,324))
    print(is_permutations2(10648000,40001688))

