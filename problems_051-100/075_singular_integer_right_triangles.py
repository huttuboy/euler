#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #75 Singular integer right triangles
#
# Author: Tuomo Huttu
# Date:   4.8.2019 (syntax updated 21.5.2020)

# Right answer: 161667 (1.11s)
# (https://en.wikipedia.org/wiki/Pythagorean_triple)

# a=m^2-n^2  b=2mn  c=m^2+n^2
# a=k(m^2-n^2)  b=2kmn  c=k(m^2+n^2)  m>n>0  (m+n)%2==1  gcd(m,n)==1
# L=a+b+c

# c_max = 1500000/2 => m_max = sqrt(750000) = 867


import time
from fractions import gcd


def main():
    limit=1500000
    
    kolmiot=[]
    for i in range(limit+1):
        kolmiot.append(0)

    for m in range(2,868):
        #if m%10==0:
        #    print(m)
        for n in range(1,m):
            #a=m*m-n*n
            #b=2*m*n
            #c=m*m+n*n
            #L=a+b+c
            L=2*(m*m+m*n)
            if L>limit:
                break
            if (m+n)%2==1 and gcd(m,n)==1:
                for k in range(1,limit):
                    if k*L<=limit:
                        kolmiot[k*L]+=1
                    else:
                        break
    
    return kolmiot.count(1)



if __name__ == "__main__":
    start_time=time.time()
    print(main())
    print("time: "+str(round(time.time()-start_time,3))+"s")

