#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #79 Passcode derivation
#
# Author: Tuomo Huttu
# Date:   28.12.2019 (syntax updated 21.5.2020)

# Right answer: 73162890 (60s)



import time


def main(x):
    L=read_file("p079_keylog.txt")
    #print(L)
    
    # 1 0 2 3 6 7 8 9
    i=10236789
    while i<x:
        i+=1
        if i%100000==0:
            print(i)
        
        s=str(i)
        ok=True
        for num in ["4","5"]:
            if str(num) in s:
                ok=False
                break
        if not ok:
            continue

        for num in ["0","1","2","3","6","7","8","9"]:
            if num not in s:
                ok=False
                break
        if not ok:
            continue

        for li in L:
            a1=s.find(li[0])
            a2=s[a1+1:].find(li[1])
            if a2<0:
                ok=False
                break
            a3=s[a2+1:].find(li[2])
            if a3<0:
                ok=False
                break
        if ok:
            return s




def read_file(file):
    L=[]
    f = open(file, "r")
    for line in f:
        l2=int(line)
        if l2 not in L:
            L.append(l2)
    f.close()
    L.sort()

    L2=[]
    for li in L:
        L2.append(str(li))
    
    return L2


if __name__ == "__main__":
    start_time=time.time()
    print(main(99999999))
    print("time: "+str(round(time.time()-start_time,3))+"s")

