#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #83 Path sum: four ways
#
# Author: Tuomo Huttu
# Date:   28.12.2019 (syntax updated 21.5.2020)

# Right answer: 425185 (0.21s)


import time


def main():
    #taul=[[131,673,234,103,18],[201,96,342,965,150],[630,803,746,422,111],[537,699,497,121,956],[805,732,524,37,331]] #2297
    taul=read_file("p083_matrix.txt")
    #print taul

    mod=[]
    for t in taul:
        mod.append(list(t))

    taul2=[]
    for r in taul:
        a=[]
        for r2 in r:
            a.append(0)
        taul2.append(a)
    taul2[0][0]=1
    #print taul2

    i_max=len(taul)
    j_max=len(taul[0])

    last_matrix=""
    while True:
        for i in range(i_max):
            for j in range(j_max):
                mod[i][j]=taul[i][j]+min_finder(mod,taul2,i,j,i_max-1,j_max-1)
                
                if not taul2[i][j] and mod[i][j]!=taul[i][j]:
                    taul2[i][j]=1
        new_matrix=str(mod)
        if new_matrix==last_matrix:
            break
        last_matrix=new_matrix
    
    return mod[i_max-1][j_max-1]

def min_finder(mod,taul2,i,j,i_max,j_max):
    if i>0 or j>0:
        aa=[]

        #upper
        if i>0 and taul2[i-1][j]:
            aa.append(mod[i-1][j])
        #left
        if j>0 and taul2[i][j-1]:
            aa.append(mod[i][j-1])
        #down
        if i<i_max and taul2[i+1][j]:
            aa.append(mod[i+1][j])
        #right
        if j<j_max and taul2[i][j+1]:
            aa.append(mod[i][j+1])
        
        if len(aa)>0:
            return min(aa)

    return 0


def read_file(file):
    L=[]
    f = open(file, "r")
    for line in f:
        l2=line.split(",")
        l3=[]
        for li in l2:
            l3.append(int(li))
        L.append(l3)
    f.close()
    
    return L

if __name__ == "__main__":
    start_time=time.time()
    print(main())
    print("time: "+str(round(time.time()-start_time,3))+"s")

