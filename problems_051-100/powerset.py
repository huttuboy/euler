#!/usr/bin/env python
# -*- coding: utf-8 -*-


import time
from itertools import chain, combinations


def powerset(iterable):
    s = list(iterable)
    return list(chain.from_iterable(combinations(s, r) for r in range(len(s)+1)))[1:]

def powerset2(L,n):
    return list(combinations(L,n))


if __name__ == "__main__":
    start_time=time.time()
    print(powerset([1,2,3]))
    print(powerset2([1,2,3],2))
    print("time: "+str(round(time.time()-start_time,3))+"s")

