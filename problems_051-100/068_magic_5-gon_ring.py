#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #68 Magic 5-gon ring
#
# Author: Tuomo Huttu
# Date:   3.8.2019 (syntax updated 21.5.2020)

# Right answer: 6531031914842725 (8.9s)



import time
from itertools import permutations as perm


def main():
    cand=[]
    for S in range(13,21):
        nums=[1,2,3,4,5,6,7,8,9,10]
        perms=get_perms(nums,3)
        p16=[]
        for p in perms:
            if sum(p)==S:
                p16.append(p)
        
        p16_2=[]
        for p in p16:
            nums2=[]
            for n in nums:
                if n not in p:
                    nums2.append(n)
            
            perms2=get_perms(nums2,7)
            for p2 in perms2:
                P=p+p2
                if P[2]+P[3]+P[4]==S and P[4]+P[5]+P[6]==S and P[6]+P[7]+P[8]==S and P[1]+P[8]+P[9]==S:
                    p16_2.append(P)
        for p in p16_2:
            if p[0]==min([p[0],p[3],p[5],p[7],p[9]]):
                apuc=str(p[0])+str(p[1])+str(p[2])+str(p[3])+str(p[2])+str(p[4])+str(p[5])+str(p[4])+str(p[6])+str(p[7])+str(p[6])+str(p[8])+str(p[9])+str(p[8])+str(p[1])
            elif p[3]==min([p[0],p[3],p[5],p[7],p[9]]):
                apuc=str(p[3])+str(p[2])+str(p[4])+str(p[5])+str(p[4])+str(p[6])+str(p[7])+str(p[6])+str(p[8])+str(p[9])+str(p[8])+str(p[1])+str(p[0])+str(p[1])+str(p[2])
            elif p[5]==min([p[0],p[3],p[5],p[7],p[9]]):
                apuc=str(p[5])+str(p[4])+str(p[6])+str(p[7])+str(p[6])+str(p[8])+str(p[9])+str(p[8])+str(p[1])+str(p[0])+str(p[1])+str(p[2])+str(p[3])+str(p[2])+str(p[4])
            elif p[7]==min([p[0],p[3],p[5],p[7],p[9]]):
                apuc=str(p[7])+str(p[6])+str(p[8])+str(p[9])+str(p[8])+str(p[1])+str(p[0])+str(p[1])+str(p[2])+str(p[3])+str(p[2])+str(p[4])+str(p[5])+str(p[4])+str(p[6])
            elif p[9]==min([p[0],p[3],p[5],p[7],p[9]]):
                apuc=str(p[9])+str(p[8])+str(p[1])+str(p[0])+str(p[1])+str(p[2])+str(p[3])+str(p[2])+str(p[4])+str(p[5])+str(p[4])+str(p[6])+str(p[7])+str(p[6])+str(p[8])
            if len(apuc)==16:
                cand.append(int(apuc))
        
    return max(cand)


def get_perms(L,rep):
    aa='0123456789'
    ret=[]
    apu=perm(aa[:len(L)],rep)
    
    for c in list(apu):
        apu2=[]
        for j in range(rep):
            apu2.append(L[int(c[j])])
        ret.append(apu2)
    return ret



if __name__ == "__main__":
    start_time=time.time()
    print(main())
    print("time: "+str(round(time.time()-start_time,3))+"s")

