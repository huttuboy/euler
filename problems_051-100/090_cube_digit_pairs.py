#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #90 Cube digit pairs
#
# Author: Tuomo Huttu
# Date:   2.1.2020 (syntax updated 21.5.2020)

# Right answer: 1217 (0.1s)


import time

distinct_dices=[]


def main():
    dice1()
    global distinct_dices
    return len(distinct_dices)


def dice1():
    for n2 in range(1,6):
        for n3 in range(n2+1,7):
            for n4 in range(n3+1,8):
                for n5 in range(n4+1,9):
                    for n6 in range(n5+1,10):
                        N1=[0,n2,n3,n4,n5,n6]
                        dice2(N1)

def dice2(N1):
    for n1 in range(0,5):
        for n2 in range(n1+1,6):
            for n3 in range(n2+1,7):
                for n4 in range(n3+1,8):
                    for n5 in range(n4+1,9):
                        for n6 in range(n5+1,10):
                            N2=[n1,n2,n3,n4,n5,n6]
                            test_dices(N1,N2)

def test_dices(N1,N2):
    if not test_pair(0,1,N1,N2):
        return
    if not test_pair(0,4,N1,N2):
        return
    if (not test_pair(0,9,N1,N2)) and (not test_pair(0,6,N1,N2)):
        return
    if (not test_pair(1,6,N1,N2)) and (not test_pair(1,9,N1,N2)):
        return
    if not test_pair(2,5,N1,N2):
        return
    if (not test_pair(3,6,N1,N2)) and (not test_pair(3,9,N1,N2)):
        return
    if (not test_pair(4,9,N1,N2)) and (not test_pair(4,6,N1,N2)):
        return
    #if (not test_pair(6,4,N1,N2)) and (not test_pair(9,4,N1,N2)): #sama kuin aiempi
    #    return
    if not test_pair(8,1,N1,N2):
        return
    
    #add to distinct list
    global distinct_dices
    aa=[N1,N2]
    aa.sort()
    aa=str(aa)
    if aa not in distinct_dices:
        distinct_dices.append(aa)
        #print aa
    
    return

def test_pair(s1,s2,N1,N2):
    if s1 in N1 and s2 in N2:
        return True
    elif s2 in N1 and s1 in N2:
        return True
    else:
        return False



if __name__ == "__main__":
    start_time=time.time()
    print(main())
    print("time: "+str(round(time.time()-start_time,3))+"s")

