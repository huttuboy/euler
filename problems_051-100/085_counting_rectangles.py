#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #85 Counting rectangles
#
# Author: Tuomo Huttu
# Date:   29.12.2019 (syntax updated 21.5.2020)

# Right answer: 2772 (8.1s) (python2.7 6.2s)


import time


def main():
    last_ero=9999999
    x_mem=0
    y_mem=0

    x=0
    rect=0
    while x<2000:
        x+=1
        y=0
        while True:
            y+=1
            rect=rect_count(x,y)
            if rect>2000000:
                new_ero=rect-2000000
                if new_ero<last_ero:
                    last_ero=new_ero
                    x_mem=x
                    y_mem=y
                    #print str(x)+" "+str(y)+" "+str(rect)
                break
            elif rect>1998900:
                new_ero=2000000-rect
                if new_ero<last_ero:
                    last_ero=new_ero
                    x_mem=x
                    y_mem=y
                    #print str(x)+" "+str(y)+" "+str(rect)
    return x_mem*y_mem




def rect_count(x,y):
    c=0
    for i in range(1,x+1):
        for j in range(1,y+1):
            c+=(x+1-i)*(y+1-j)
    return c


if __name__ == "__main__":
    start_time=time.time()
    print(main())
    print("time: "+str(round(time.time()-start_time,3))+"s")

