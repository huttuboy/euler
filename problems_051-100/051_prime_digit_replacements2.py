#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #51 Prime digit replacements
#
# Author: Tuomo Huttu
# Date:   12.1.2020 (syntax updated 21.5.2020)

# Right answer: 121313 (0.54) (python2.7 0.3s)


import time
from primes import is_prime


def main():
    i=56003
    while True:
        i+=2
        if i%5==0:
            i+=2
        str_i=str(i)
        z=0
        if str_i[0]=="1":
            z=1
        for k in range(z,3):
            if str(k) in str_i:
                not_prime_count=0
                if z:
                    not_prime_count=1
                for j in range(z,10):
                    if not is_prime(int(str_i.replace(str(k),str(j)))):
                        not_prime_count+=1
                        if not_prime_count>2:
                            break
                if not_prime_count<3:
                    return i
    
    return



if __name__ == "__main__":
    start_time=time.time()
    print(main())
    print("time: "+str(round(time.time()-start_time,3))+"s")

