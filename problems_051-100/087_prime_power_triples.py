#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #87 Prime power triples
#
# Author: Tuomo Huttu
# Date:   31.12.2019 (syntax updated 21.5.2020)

# Right answer: 1097343 (9.1s) (python2.7 6.5s)


import time
from primes import *


def main(x):
    prim=get_primelist(upperlimit=int(x**.5))
    L=[]
    i=0
    while i<x:
        i+=1
        L.append(0)
    
    prim_le=len(prim)
    for i in range(prim_le): #**4
        s4=prim[i]**4
        if s4>=x:
            break
        for j in range(prim_le): #**3
            s3=prim[j]**3
            if s4+s3>=x:
                break
            for k in range(prim_le): #**2
                su=s4+s3+prim[k]**2
                if su>=x:
                    break
                elif not L[su]:
                    L[su]=1
                    #print str(su)
    return sum(L)



if __name__ == "__main__":
    start_time=time.time()
    #print str(main(50)) #4 ok
    print(main(50000000))
    print("time: "+str(round(time.time()-start_time,3))+"s")

