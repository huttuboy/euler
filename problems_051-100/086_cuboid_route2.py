#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #86 Cuboid route
#
# Author: Tuomo Huttu
# Date:   30.12.2019 (syntax updated 21.5.2020)

# Right answer: 1818 (4.7s) (python2.7 2.2s)


import time
from triangular import *


def main(x):
    c=0
    A=0
    while True:
        A+=1
        #if A%100==0:
        #    print str(A)
        B=1
        while B<2*A:
            B+=1
            if is_square(A**2+B**2):
                c+=min(A,B-1)-(B+1)/2+1
                lis=min(A,B-1)-(B+1)/2+1
                #print str(A)+" "+str(B)+"   "+str(lis)
                if c>=x:
                    return A

    return 0




if __name__ == "__main__":
    start_time=time.time()
    #print str(main(10))
    #print str(main(99))  #1975
    #print str(main(100)) #2060
    
    #print str(main(20000)) #100
    print(main(1000000))
    print("time: "+str(round(time.time()-start_time,3))+"s")

