#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #99 Largest exponential
#
# Author: Tuomo Huttu
# Date:   5.1.2020 (syntax updated 22.5.2020)

# Right answer: 709 (0.003s)


import time,math


def main():
    aa=read_file("p099_base_exp.txt")
    
    suurin=0
    ret=0
    for i in range(len(aa)):
        b=aa[i][1]*math.log(aa[i][0])
        if b>suurin:
            suurin=b
            ret=i+1
    
    return ret


def read_file(file):
    ret=[]
    f = open(file, "r")
    for line in f:
        nums=line.split(",")
        ret.append([int(nums[0]),int(nums[1])])
    f.close()
    return ret



if __name__ == "__main__":
    start_time=time.time()
    print(main())
    print("time: "+str(round(time.time()-start_time,3))+"s")

