#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #53 Combinatoric selections
#
# Author: Tuomo Huttu
# Date:   12.1.2020 (syntax updated 21.5.2020)

# Right answer: 4075 (0.012s)


import time
from math import factorial


def main():
    ret=0
    for n in range(1,101):
        for r in range(1,n+1):
            if nCr(n,r)>1000000:
                ret+=1
    
    return ret


def nCr(n,r):
    return factorial(n)/(factorial(r)*factorial(n-r))



if __name__ == "__main__":
    start_time=time.time()
    print(main())
    print("time: "+str(round(time.time()-start_time,3))+"s")

