#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #98 Anagramic squares
#
# Author: Tuomo Huttu
# Date:   5.1.2020 (syntax updated 22.5.2020)

# Right answer: 18769 (43s) (python2.7 34s)


import time
from triangular import *


def main():
    words=read_file("p098_words.txt")

    anagrams=[]
    for i in range(len(words)-1):
        for j in range(i+1,len(words)):
            if is_anagram(words[i],words[j]):
                anagrams.append([words[i],words[j]])
    
    biggest=0
    for a in anagrams:
        print(a[0]+" "+a[1])
        chars=list(set(a[0]))
        aa=square_test(a[0],a[1],chars,[])
        if aa>biggest:
            biggest=aa
    
    return biggest


def square_test(w1,w2,chars,nums):
    if len(nums)==0:
        biggest=0
        for i in range(1,10):
            nums2=list(nums)
            aa=square_test(w1,w2,chars,nums2+[i])
            if aa>biggest:
                biggest=aa
        return biggest
    elif len(nums)<len(chars):
        biggest=0
        for i in range(0,10):
            if i==0 and (chars[len(nums)]==w1[0] or chars[len(nums)]==w2[0]):
                continue
            if i in nums:
                continue
            nums2=list(nums)
            aa=square_test(w1,w2,chars,nums2+[i])
            if aa>biggest:
                biggest=aa
        return biggest
    else:
        num=w1
        for i in range(len(chars)):
            num=num.replace(chars[i],str(nums[i]))
        num=int(num)
        if not is_square(num):
            return 0
        
        num2=w2
        for i in range(len(chars)):
            num2=num2.replace(chars[i],str(nums[i]))
        num2=int(num2)
        if not is_square(num2):
            return 0
        
        return max(num,num2)


def is_anagram(a,b):
    if len(a)!=len(b):
        return False
    a2=list(a)
    b2=list(b)
    a2.sort()
    b2.sort()
    if a2==b2:
        return True
    return False


def read_file(file):
    ret=[]
    f = open(file, "r")
    for line in f:
        sanat_h=line.split(",")
        for s in sanat_h:
            a=s.replace('"',"")
            ret.append(a)
    f.close()
    return ret



if __name__ == "__main__":
    start_time=time.time()
    print(main())
    print("time: "+str(round(time.time()-start_time,3))+"s")

