#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #65 Convergents of e
#
# Author: Tuomo Huttu
# Date:   3.5.2018 (syntax updated 21.5.2020)

# Right answer: 272 (0.002s)


import time
from fractions import Fraction

a=[]


def main(x):
    global a

    i=1
    while len(a)<x:
        a.append(1)
        a.append(2*i)
        a.append(1)
        i+=1
    
    aa=str(2+fraq(0,x-1)).split("/")[0]
    ret=0
    for i in range(len(aa)):
        ret+=int(aa[i])
    
    return ret


def fraq(i,total):
    global a
    if i<total-1:
        return Fraction(1,a[i]+fraq(i+1,total))
    else:
        return Fraction(1,a[i])



if __name__ == "__main__":
    start_time=time.time()
    print(main(100))
    print("time: "+str(round(time.time()-start_time,3))+"s")

