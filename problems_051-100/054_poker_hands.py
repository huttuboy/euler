#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #54 Poker hands
#
# Author: Tuomo Huttu
# Date:   20.4.2018 (syntax updated 21.5.2020)

# Right answer: 376 (0.04s)



import time
from poker_list import *


def main():
    p=get_poker_list()
    p=p.split(",")

    player1_wins=0
    for i in range(len(p)):
        k=p[i].split(" ")
        a1=k[0:5]
        a1.sort()
        a2=k[5:10]
        a2.sort()
        if hand_value(a1)>hand_value(a2):
            player1_wins+=1
    
    return player1_wins



def hand_value(H): #T=10 J=11 Q=12 K=13 A=14
    #royalflush  =9 99 14 00 0000
    #straighflush=9 99 bn 00 0000
    #fourkind    =9 88 nu 00 0000
    #fullhouse   =8 99 3s 00 0000
    #flush       =7 bn 2n 3n 4n5n
    #straight    =6 bn 00 00 0000
    #3kind       =5 bn 00 00 0000
    #2pairs      =4 bn 2n 3n 0000
    #pair        =3 bn 2n 3n 4n00
    #high        =2 bn 2n 3n 4n5n
    nums=[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
    suits=[0,0,0,0]

    for c in H:
        n=c[0]
        if n in ["2","3","4","5","6","7","8","9"]:
            nums[int(n)]+=1
        elif n=="T":
            nums[10]+=1
        elif n=="J":
            nums[11]+=1
        elif n=="Q":
            nums[12]+=1
        elif n=="K":
            nums[13]+=1
        elif n=="A":
            nums[14]+=1
        s=c[1]
        if s=="C":
            suits[0]+=1
        elif s=="D":
            suits[1]+=1
        elif s=="H":
            suits[2]+=1
        elif s=="S":
            suits[3]+=1
    if "11111" in combine(nums) and 5 in suits:
        i=14
        while True:
            if nums[i]>0:
                break
            i-=1
        if i<10:
            return int("9990"+str(i)+"000000")
        return int("999"+str(i)+"000000")
    elif 4 in nums:
        i=14
        while True:
            if nums[i]>3:
                break
            i-=1
        if i<10:
            return int("9880"+str(i)+"000000")
        return int("988"+str(i)+"000000")
    elif 3 in nums and 2 in nums:
        i=14
        while True:
            if nums[i]==3:
                break
            i-=1
        if i<10:
            return int("8990"+str(i)+"000000")
        return int("899"+str(i)+"000000")
    elif 5 in suits:
        ret="7"
        i=14
        while True:
            if nums[i]==1:
                if i<10:
                    ret+="0"
                ret+=str(i)
            i-=1
            if i<2:
                break
        return int(ret)
    elif "11111" in combine(nums):
        i=14
        while True:
            if nums[i]>0:
                break
            i-=1
        if i<10:
            return int("60"+str(i)+"00000000")
        return int("6"+str(i)+"00000000")
    elif 3 in nums:
        ret="5"
        i=14
        while True:
            if nums[i]==3:
                break
            i-=1
        if i<10:
            ret+="0"
        ret+=str(i)
        return int(ret+"00000000")
    elif 2 in nums:
        pari_lkm=0
        for i in range(len(nums)):
            if nums[i]==2:
                pari_lkm+=1
        if pari_lkm==2:
            ret="4"
            i=14
            while True:
                if nums[i]==2:
                    if i<10:
                        ret+="0"
                    ret+=str(i)
                i-=1
                if i<2:
                    break
            i=14
            while True:
                if nums[i]==1:
                    if i<10:
                        ret+="0"
                    ret+=str(i)
                    break
                i-=1
            return int(ret+"0000")
        elif pari_lkm==1:#3 bn 2n 3n 4n00
            ret="3"
            i=14
            while True:
                if nums[i]==2:
                    if i<10:
                        ret+="0"
                    ret+=str(i)
                    break
                i-=1
            i=14
            while True:
                if nums[i]==1:
                    if i<10:
                        ret+="0"
                    ret+=str(i)
                i-=1
                if i<2:
                    break
            return int(ret+"00")
    ret="2"
    i=14
    while True:
        if nums[i]==1:
            if i<10:
                ret+="0"
            ret+=str(i)
        i-=1
        if i<2:
            break
    return int(ret)


def combine(L):
    ret=""
    for i in L:
        ret+=str(i)
    return ret


if __name__ == "__main__":
    start_time=time.time()
    print(main())
    print("time: "+str(round(time.time()-start_time,3))+"s")

