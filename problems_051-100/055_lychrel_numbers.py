#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #55 Lychrel numbers
#
# Author: Tuomo Huttu
# Date:   20.4.2018 (syntax updated 21.5.2020)

# Right answer: 249 (0.062s)


import time
from is_palindrome import is_palindrome
from reverse import reverse


def main():
    ret=0
    for i in range(1,10000):
        if is_lychrel(i):
            ret+=1
    
    return ret


def is_lychrel(x): # x<10000
    a=x
    for i in range(50):
        #a+=long(reverse(a))
        a+=int(reverse(a))
        if is_palindrome(a):
            #print str(x)+" "+str(i+1)+" "+str(a)
            return False
    return True



if __name__ == "__main__":
    start_time=time.time()
    print(main())
    print("time: "+str(round(time.time()-start_time,3))+"s")

