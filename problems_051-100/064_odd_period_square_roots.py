#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #64 Odd period square roots
#
# Author: Tuomo Huttu
# Date:   3.5.2018 (syntax updated 21.5.2020)

# Right answer: 1322 (5.7s) (python2.7 4.2s)


import time
from triangular import is_square
from math import floor

aa=50


def main(x):
    global aa

    ret=0
    i=0
    while i<=x:
        if not is_square(i):
            a=period_length(period(i))
            if a==0:
                aa+=100
                continue
            elif aa>=60:
                aa-=10
            if a%2==1:
                ret+=1
                #print str(i)
        i+=1

    return ret


def period(x):
    global aa

    ret=[]
    a=int(floor(x**0.5))
    b=1
    for i in range(aa):
        b2=x-a**2
        b=b2/b
        r=int(floor((x**0.5+a)/b))
        ret.append(r)
        a=r*b-a
    return ret


def period_length(x):
    global aa
    
    ok=True
    for i in range(1,17):
        if x[0]!=x[i]:
            ok=False
            break
    if ok:
        return 1
    
    ok=True
    for i in range(1,13):
        if x[0:2]!=x[2*i:2*i+2]:
            ok=False
            break
    if ok:
        return 2
    
    ok=True
    for i in range(1,9):
        if x[0:3]!=x[3*i:3*i+3]:
            ok=False
            break
    if ok:
        return 3
    
    #for j in range(4,aa/4-2):
    for j in range(4,aa//4-2):
        ok=True
        for i in range(1,5):
            if x[0:j]!=x[j*i:j*i+j]:
                ok=False
                break
        if ok:
            return j
    return 0



if __name__ == "__main__":
    start_time=time.time()
    print(main(10000))
    #print str(main(13))
    print("time: "+str(round(time.time()-start_time,3))+"s")

