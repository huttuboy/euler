#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #52 Permuted multiples
#
# Author: Tuomo Huttu
# Date:   12.1.2020 (syntax updated 21.5.2020)

# Right answer: 142857 (0.008s)


import time
from itertools import permutations


def main():
    perm=list(permutations("23456789",5))
    for p in perm:
        a1="1"+"".join(p)
        int_a1=int(a1)
        a6=str(6*int_a1)
        if len(a1)==len(a6):
            c1=list(a1)
            c1.sort()
            for k in range(2,6):
                c2=list(str(k*int_a1))
                c2.sort()
                if c1!=c2:
                    break
                elif k==5:
                    return int_a1
    
    return



if __name__ == "__main__":
    start_time=time.time()
    print(main())
    print("time: "+str(round(time.time()-start_time,3))+"s")

