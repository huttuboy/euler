#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #95 Amicable chains
#
# Author: Tuomo Huttu
# Date:   4.1.2020 (syntax updated 22.5.2020)

# Right answer: 14316 (2.9s) (python2.7 >6min)


import time
from powerset import powerset
from primes import *
from product import product


def main():
    #apuapu(203034)
    #return
    len_list=[]
    i=0
    while i<1000000:
        len_list.append(-1)
        i+=1
    
    i=4
    while i<10000:
        if i%1000==0:
            print(i)
        if len_list[i]>=0:
            i+=2
            continue
        ch_list=[]
        j=0
        ds=i
        while True:
            ds=div_sum(ds)
            if ds==1 or ds>999999:
                len_list[i]=0
                for c in ch_list:
                    len_list[c]=0
                break
            elif ds==i:
                le=len(ch_list)+1
                len_list[i]=le
                for c in ch_list:
                    len_list[c]=le
                break
            elif ds in ch_list:
                len_list[i]=0
                ind=ch_list.index(ds)
                le=len(ch_list)-ind
                for c in ch_list[ind:]:
                    len_list[c]=le
                break
            elif len_list[ds]>=0:
                len_list[i]=0
                for c in ch_list:
                    len_list[c]=0
                break
            ch_list.append(ds)
        i+=2

    m=max(len_list)
    print(m)
    return len_list.index(m)


def apuapu(x):
    a=x
    for i in range(1,200):
        d=div_sum(a)
        print(str(i)+" "+str(a)+" "+str(d))
        a=d
        if d==x or d==1:
            break
    return


def div_sum(x):
    pr=primes(x)
    a=[]
    a.append(1)
    ps=powerset(pr)
    for p in ps:
        aa=product(p)
        if aa not in a and aa!=x:
            a.append(aa)
    return sum(a)



if __name__ == "__main__":
    start_time=time.time()
    print(main())
    print("time: "+str(round(time.time()-start_time,3))+"s")

