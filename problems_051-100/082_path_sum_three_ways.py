#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #82 Path sum: three ways
#
# Author: Tuomo Huttu
# Date:   28.12.2019 (syntax updated 21.5.2020)

# Right answer: 260324 (0.014s)


import time


def main():
    #taul=[[131,673,234,103,18],[201,96,342,965,150],[630,803,746,422,111],[537,699,497,121,956],[805,732,524,37,331]] #994
    taul=read_file("p082_matrix.txt")
    #print taul

    for j in range(len(taul[0])):
        if j>0:
            col_lower=[]
            for c in range(len(taul)):
                if c==0:
                    col_lower.append(taul[len(taul)-1][j-1]+taul[len(taul)-1][j])
                else:
                    col_lower.append(taul[len(taul)-1-c][j]+ min([taul[len(taul)-1-c][j-1],col_lower[c-1]]))
            col_lower.reverse()

        for i in range(len(taul)):
            if j>0:
                if i==0:
                    taul[i][j]+=min([taul[i][j-1],col_lower[i+1]])
                elif i==len(taul)-1:
                    taul[i][j]+=min([taul[i][j-1],taul[i-1][j]])
                else:
                    taul[i][j]+=min([taul[i][j-1],col_lower[i+1],taul[i-1][j]])
    
    last_col=[]
    for r in taul:
        last_col.append(r[len(r)-1])
    
    return min(last_col)


def read_file(file):
    L=[]
    f = open(file, "r")
    for line in f:
        l2=line.split(",")
        l3=[]
        for li in l2:
            l3.append(int(li))
        L.append(l3)
    f.close()
    
    return L



if __name__ == "__main__":
    start_time=time.time()
    print(main())
    print("time: "+str(round(time.time()-start_time,3))+"s")

