#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #81 Path sum: two ways
#
# Author: Tuomo Huttu
# Date:   28.12.2019 (syntax updated 21.5.2020)

# Right answer: 427337 (0.006s)


import time


def main():
    #taul=[[131,673,234,103,18],[201,96,342,965,150],[630,803,746,422,111],[537,699,497,121,956],[805,732,524,37,331]] #2427
    taul=read_file("p081_matrix.txt")
    #print taul

    for i in range(len(taul)):
        for j in range(len(taul[0])):
            if i>0 and j>0:
                if taul[i-1][j]<taul[i][j-1]:
                    taul[i][j]+=taul[i-1][j]
                else:
                    taul[i][j]+=taul[i][j-1]
            elif i>0:
                taul[i][j]+=taul[i-1][j]
            elif j>0:
                taul[i][j]+=taul[i][j-1]
    
    return taul[len(taul)-1][len(taul[0])-1]



def read_file(file):
    L=[]
    f = open(file, "r")
    for line in f:
        l2=line.split(",")
        l3=[]
        for li in l2:
            l3.append(int(li))
        L.append(l3)
    f.close()
    
    return L

if __name__ == "__main__":
    start_time=time.time()
    print(main())
    print("time: "+str(round(time.time()-start_time,3))+"s")

