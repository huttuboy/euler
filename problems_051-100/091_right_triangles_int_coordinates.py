#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #91 Right triangles with integer coordinates
#
# Author: Tuomo Huttu
# Date:   2.1.2020 (syntax updated 21.5.2020)

# Right answer: 14234 (10.9s) (python2.7 5.3s)


import time,math


def main(N):
    coords=get_coords(N)
    le=len(coords)
    #print coords
    #return le
    sq=0
    for P in range(0,le-1):
        for Q in range(P+1,le):
            if is_R_angled(coords[P],coords[Q]):
                sq+=1
    return sq


def is_R_angled(P,Q):
    x1,y1=P
    x2,y2=Q
    
    if x1+x2==0 or y1+y2==0:
        #print "x1="+str()+" y1="+str(y1)+" x2="+str(x2)+" y2="+str(y2)
        return False
    if x1+y2==0 or x2+y1==0:
        return True
    if x1==0 and y1==y2:
        return True
    if x2==0 and y1==y2:
        return True
    if y1==0 and x1==x2:
        return True
    if y2==0 and x1==x2:
        return True
    j=[]
    j.append((x2-x1)**2+(y2-y1)**2)
    j.append((x2-0)**2+(y2-0)**2)
    j.append((0-x1)**2+(0-y1)**2)
    j.sort()
    if j[0]+j[1]==j[2]:
        return True
    return False


def get_coords(N):
    co=[]
    for x in range(N+1):
        for y in range(N+1):
            if x+y>0:
                co.append([x,y])
    return co



if __name__ == "__main__":
    start_time=time.time()
    print(main(50))
    print("time: "+str(round(time.time()-start_time,3))+"s")

