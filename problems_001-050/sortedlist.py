#!/usr/bin/env python
# -*- coding: utf-8 -*-


from bisect import bisect_left,insort


def is_in_sortedlist(L,x):
    i=bisect_left(L,x)
    if i<len(L) and L[i]==x:
        return True
    return False


def append_sortedlist(L,x):
    insort(L,x)
    return



if __name__ == "__main__":
    a=[2, 3, 4, 4, 5, 8, 12, 36, 36, 36, 85, 89, 96]

    print(is_in_sortedlist(a,2))
    print(is_in_sortedlist(a,8))
    print(is_in_sortedlist(a,96))

    print(is_in_sortedlist(a,9))
    print(is_in_sortedlist(a,-7))
    print(is_in_sortedlist(a,1000))

    append_sortedlist(a,200)
    append_sortedlist(a,10)
    append_sortedlist(a,5)
    append_sortedlist(a,1)
    print(a)

