#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #50 Consecutive prime sum
#
# Author: Tuomo Huttu
# Date:   20.4.2018 (syntax updated 21.5.2020)

# Right answer: 997651 (0.005s)


import time
from primes import *

prime_list=[2,3]


def main(x):
    global prime_list
    
    last=prime_list[len(prime_list)-1]
    
    while sum(prime_list)<x:
        last=new_prime(last)
        if last<x:
            prime_list.append(last)
    
    ret=0
    le=len(prime_list)
    for i in range(le):
        for j in range(i+1):
            La=prime_list[j:le+j-i]
            s=sum(La)
            if is_prime(s):
                ret=s
                print(len(La))
                break
        if ret>0:
            break
    
    return ret



if __name__ == "__main__":
    start_time=time.time()
    print(main(1000000))
    print("time: "+str(round(time.time()-start_time,3))+"s")

