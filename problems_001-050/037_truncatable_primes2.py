#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #37 Truncatable primes
#
# Author: Tuomo Huttu
# Date:   10.1.2020 (syntax updated 21.5.2020)

# Right answer: 748317 (5.4s) (python2.7 4.2s)


import time
from primes import *

prime_list=[]


def main():
    global prime_list
    
    prime_list=get_primelist(0,800000)
    
    ret=[23]
    
    for p in prime_list:
        if p<24:
            continue
        if is_truncatable(p):
            #print str(p)
            ret.append(p)
            if len(ret)==11:
                break
    
    return sum(ret)


def is_truncatable(x):
    a=str(x)
    
    if "0" in a or "2" in a or "4" in a or "6" in a or "8" in a:
        return False

    global prime_list
    for i in range(1,len(a)):
        if int(a[i:]) not in prime_list or int(a[0:i]) not in prime_list:
            return False
    
    return True



if __name__ == "__main__":
    start_time=time.time()
    print(main())
    print("time: "+str(round(time.time()-start_time,3))+"s")

