#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #47 Distinct primes factors
#
# Author: Tuomo Huttu
# Date:   17.4.2018 (syntax updated 21.5.2020)

# Right answer: 134043 (35s) (python2.7 24s)


import time
from primes import *


def main(x):
    i=2
    ret=0
    z=x
    while ret==0:
        i+=z
        if z==x and is_consecutive_count_n(primes(i),x):
            i-=x
            z=1
        elif z==1:
            k=0
            while z==1:
                j=0
                while j<x:
                    if is_consecutive_count_n(primes(i),x):
                        #print str(i)+" "+str(is_consecutive_count_n(primes(i),x))
                        if j==0:
                            ret=i
                        j+=1
                    else:
                        ret=0
                        break
                    i+=1
                k+=1
                if k>2*x-1 or ret>0:
                    z=x
    
    return ret


def is_consecutive_count_n(L,n):
    ret=[]
    for a in L:
        if a not in ret:
            ret.append(a)
            if len(ret)>n:
                return False
    
    if len(ret)==n:
        return True
    return False



if __name__ == "__main__":
    start_time=time.time()
    print(main(4))
    print("time: "+str(round(time.time()-start_time,3))+"s")

