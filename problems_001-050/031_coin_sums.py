#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #31 Coin sums
#
# Author: Tuomo Huttu
# Date:   14.4.2018 (syntax updated 21.5.2020)

# Right answer: 73682 (0.027s)


import time


def main():
    ret=1 #1x200p
    
    for p100 in range(0,3):
        for p50 in range(0,5):
            if p100*100+p50*50>200:
                break
            for p20 in range(0,11):
                if p100*100+p50*50+p20*20>200:
                    break
                for p10 in range(0,21):
                    if p100*100+p50*50+p20*20+p10*10>200:
                        break
                    for p5 in range(0,41):
                        if p100*100+p50*50+p20*20+p10*10+p5*5>200:
                            break
                        for p2 in range(0,101):
                            if p100*100+p50*50+p20*20+p10*10+p5*5+p2*2 <=200:
                                ret+=1
                            else:
                                break
    
    return ret



if __name__ == "__main__":
    start_time=time.time()
    print(main())
    print("time: "+str(round(time.time()-start_time,3))+"s")

