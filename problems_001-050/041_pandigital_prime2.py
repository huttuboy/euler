#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #41 Pandigital prime
#
# Author: Tuomo Huttu
# Date:   16.4.2018 (syntax updated 21.5.2020)

# Right answer: 7652413 (time: 1.03s)


import time
from itertools import permutations


def main():
    ret=0
    num="987654321"
    for i in range(10):
        aa=list(permutations(num[i:]))
        for a in aa:
            if is_prime(int(combine(a))):
                ret=int(combine(a))
                break
        if ret:
            break
    return ret


def combine(L):
    ret=""
    for i in L:
        ret+=str(i)
    return ret


def is_prime(x):
    if x<=1:
        return False
    elif x<4:
        return True
    elif x%2==0 or x%3==0:
        return False
    elif x>9:
        r=int(x**0.5)
        f=5
        while f<=r:
            if x%f==0 or x%(f+2)==0:
                return False
            f+=6
    
    return True


if __name__ == "__main__":
    start_time=time.time()
    print(main())
    print("time: "+str(round(time.time()-start_time,3))+"s")

