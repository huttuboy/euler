#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #45 Triangular, pentagonal, and hexagonal
#
# Author: Tuomo Huttu
# Date:   16.4.2018 (syntax updated 21.5.2020)

# Right answer: 1533776805 (0.09s) (python2.7 0.04s)


import time


def main():
    n=1
    ret=0
    while True:
        #tri=n*(n+1)/2
        tri=n*(n+1)//2
        if is_pentagonal(tri) and is_hexagonal(tri):
            #print str(tri)
            if tri>40755:
                ret=tri
                break
        n+=1
    
    return ret


def is_pentagonal(x):
    #n=(1+(1+24*x)**0.5)/6
    #m=int(n)
    m=(1+(1+24*x)**0.5)//6
    #if x==m*(3*m-1)/2:
    if x==m*(3*m-1)//2:
        return True
    return False


def is_hexagonal(x):
    #n=(1+(1+8*x)**0.5)/4
    #m=int(n)
    m=(1+(1+8*x)**0.5)//4
    if x==m*(2*m-1):
        return True
    return False



if __name__ == "__main__":
    start_time=time.time()
    print(main())
    print("time: "+str(round(time.time()-start_time,3))+"s")

