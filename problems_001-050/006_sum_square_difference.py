#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #6 Sum square difference
#
# Author: Tuomo Huttu
# Date:   9.6.2018 (syntax updated 21.5.2020)

# Right answer: 25164150



def main(x):
    sum1=0
    sum2=0
    for i in range(1,x+1):
        sum1+=i*i
        sum2+=i
    
    return sum2**2-sum1



if __name__ == "__main__":
    print(main(100))

