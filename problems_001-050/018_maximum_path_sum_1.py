#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #18 Maximum path sum I
#
# Author: Tuomo Huttu
# Date:   10.4.2018 (syntax updated 21.5.2020)

# Right answer: 1074 (0s)


import time


tri="75,95 64,17 47 82,18 35 87 10,20 04 82 47 65,19 01 23 75 03 34,88 02 77 73 07 63 67,99 65 04 28 06 16 70 92,41 41 26 56 83 40 80 70 33,41 48 72 33 47 32 37 16 94 29,53 71 44 65 25 43 91 52 97 51 14,70 11 33 28 77 73 17 78 39 68 17 57,91 71 52 38 17 14 91 43 58 50 27 29 48,63 66 04 68 89 53 67 30 73 16 69 87 40 31,04 62 98 27 23 09 70 98 73 93 38 53 60 04 23"
#tri="3,7 4,2 4 6,8 5 9 3"


def main():
    global tri
    
    tri2=[]
    rivit=tri.split(",")
    for r in rivit:
        tri2.append(r.split(" "))
    
    i=len(tri2)-1
    while i>0:
        i-=1
        for j in range(i+1):
            if int(tri2[i+1][j])>int(tri2[i+1][j+1]):
                tri2[i][j]=int(tri2[i][j])+int(tri2[i+1][j])
            else:
                tri2[i][j]=int(tri2[i][j])+int(tri2[i+1][j+1])
    
    return tri2[0][0]



if __name__ == "__main__":
    start_time=time.time()
    print(main())
    print("time: "+str(round(time.time()-start_time,3))+"s")

