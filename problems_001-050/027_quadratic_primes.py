#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #27 Quadratic primes
#
# Author: Tuomo Huttu
# Date:   14.4.2018 (syntax updated 21.5.2020)

# Right answer: -59231 (3.8s) (python2.7 2.6s)


import time
from primes import *


def main():
    a_mem=-9999
    b_mem=-9999
    max_pr=0

    a=-999
    while a<1000:
        b=-1000
        while b<1001:
            n=0
            while True:
                if is_prime(n**2+a*n+b):
                    n+=1
                    if n>max_pr:
                        max_pr=n
                        a_mem=a
                        b_mem=b
                else:
                    break
            #print str(i)+" "+str(j)+" "+str(n)
            b+=1
        a+=1
    
    return a_mem*b_mem



if __name__ == "__main__":
    start_time=time.time()
    print(main())
    print("time: "+str(round(time.time()-start_time,3))+"s")

