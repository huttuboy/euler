#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #48 Self powers
#
# Author: Tuomo Huttu
# Date:   17.4.2018 (syntax updated 21.5.2020)

# Right answer: 9110846700 (0.011s)


import time


def main(x):
    ret=0
    for i in range(1,x+1):
        ret+=(i**i)
    ret=str(ret)
    
    return ret[len(ret)-10:]



if __name__ == "__main__":
    start_time=time.time()
    print(main(1000))
    print("time: "+str(round(time.time()-start_time,3))+"s")

