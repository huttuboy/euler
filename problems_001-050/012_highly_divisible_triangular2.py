#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #12 Highly divisible triangular number
#
# Author: Tuomo Huttu
# Date:   7.1.2020 (syntax updated 21.5.2020)

# Right answer: 76576500 (5.0s) (python2.7 3.6s)



import time
from powerset import powerset
from product import product
from primes import *



def main(x):
    a=1
    tri=0
    while True:
        tri+=a
        a+=1
        if div_count(tri)>x:
            return tri
    
    return 0



def div_count(x):
    pr=primes(x)
    a=[]
    a.append(1)
    ps=powerset(pr)
    for p in ps:
        aa=product(p)
        if aa not in a:
            a.append(aa)
    return len(a)



if __name__ == "__main__":
    start_time=time.time()
    print(main(500))
    print("time: "+str(round(time.time()-start_time,3))+"s")

