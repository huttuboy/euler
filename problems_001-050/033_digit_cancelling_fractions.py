#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #33 Digit cancelling fractions
#
# Author: Tuomo Huttu
# Date:   15.4.2018 (syntax updated 21.5.2020)

# Right answer: 100 (0.001s)


import time
from fractions import Fraction
from product import product


def main():
    num="123456789"
    nu=[]
    de=[]
    
    for i in range(9):
        a1=num[i]
        num2=num[0:i]+num[i+1:]
        for j in range(8):
            a2=num2[j]
            num3=num2[0:j]+num2[j+1:]
            for k in range(7):
                a3=num3[k]
                if float(a1+a2)/float(a2+a3)==float(a1)/float(a3):
                    #print a1+a2+"/"+a2+a3
                    nu.append(int(a1))
                    de.append(int(a3))
    #print nu
    #print de
    #print str(product(nu))+"/"+str(product(de))
    return str(Fraction(product(nu),product(de))).split("/")[1]



if __name__ == "__main__":
    start_time=time.time()
    print(main())
    print("time: "+str(round(time.time()-start_time,3))+"s")

