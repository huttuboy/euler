#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #29 Distinct powers
#
# Author: Tuomo Huttu
# Date:   14.4.2018 (syntax updated 21.5.2020)

# Right answer: 9183 (0.67s)


import time


def main():
    ret=[]
    for a in range(2,101):
        for b in range(2,101):
            aa=a**b
            if aa not in ret:
                ret.append(aa)
    return len(ret)



if __name__ == "__main__":
    start_time=time.time()
    print(main())
    print("time: "+str(round(time.time()-start_time,3))+"s")

