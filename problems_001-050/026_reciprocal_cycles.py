#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #26 Reciprocal cycles
#
# Author: Tuomo Huttu
# Date:   14.4.2018 (syntax updated 21.5.2020)

# Right answer: 983 (0.45s) (python2.7 0.36s)



import time

def main(x):
    longest=0
    value=0
    for i in range(2,x):
        #a=str(10**2000/i) #exp 2*x ?
        a=str(10**2000//i)
        re=reciprocal(a)
        if re>longest:
            longest=re
            value=i
            #print str(i)+"  "+str(re)
    
    return value


def reciprocal(x):
    if "0000000000" in x:
        return 0
    
    for i in range(len(x)):
        for j in range(1,1200):
            a=x[i:i+j]
            if j<3 and a==x[i+j:i+2*j] and a==x[i+2*j:i+3*j] and a==x[i+3*j:i+4*j] and a==x[i+4*j:i+5*j]:
                return j
            elif j>=3 and a==x[i+j:i+2*j] and a==x[i+2*j:i+3*j]:
                return j
            elif j>=10 and a==x[i+j:i+2*j]:
                return j
    
    return 0


if __name__ == "__main__":
    start_time=time.time()
    print(main(1000))
    print("time: "+str(round(time.time()-start_time,3))+"s")

