#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #5 Smallest multiple
#
# Author: Tuomo Huttu
# Date:   9.4.2018 (syntax updated 21.5.2020)

# Right answer: 232792560 (3.9s)


import time


def main(x):
    x2=x
    div=[x2]
    while x2>1:
        x2-=1
        lis_ok=True
        for d in div:
            if d%x2==0:
                lis_ok=False
                break
        if lis_ok:
            div.append(x2)
    
    ret=0
    while True:
        ret+=x
        ok=True
        for d in div:
            if ret%d!=0:
                ok=False
                break
        if ok:
            return ret


if __name__ == "__main__":
    start_time=time.time()
    print(main(20))
    print("time: "+str(round(time.time()-start_time,3))+"s")

