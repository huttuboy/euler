#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #47 Distinct primes factors
#
# Author: Tuomo Huttu
# Date:   11.1.2020 (syntax updated 21.5.2020)

# Right answer: 134043 (20.1s) (python2.7 9.0s)


import time
from primes import *


def main(x):
    i=646
    while True:
        i+=4
        if distinct_primes_len(i)==4:
            ret=i
            kpl=1

            for j in range(1,4):
                if distinct_primes_len(i-j)!=4:
                    break
                kpl+=1
                ret=i-j
                if kpl==4:
                    return ret

            for j in range(1,4):
                if distinct_primes_len(i+j)!=4:
                    i+=j
                    break
                kpl+=1
                if kpl==4:
                    return ret
    return


def distinct_primes_len(x):
    pr=primes(x)
    ret=[]
    last=0
    for p in pr:
        if p>last:
            last=p
            ret.append(p)
            if len(ret)>4:
                return 5
    return len(ret)



if __name__ == "__main__":
    start_time=time.time()
    print(main(4))
    print("time: "+str(round(time.time()-start_time,3))+"s")

