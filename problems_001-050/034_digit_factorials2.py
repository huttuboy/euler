#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #34 Digit factorials
#
# Author: Tuomo Huttu
# Date:   10.1.2020 (syntax updated 21.5.2020)

# Right answer: 40730 (8.4s)


import time
#from factorial import factorial
from math import factorial


def main():
    fa=[]
    for i in range(0,10):
        fa.append(factorial(i))
    
    ret=0
    i=10
    while i<2540161: # 7*9!=2540160
        aa=str(i)
        s=0
        for j in range(len(aa)):
            s+=fa[int(aa[j])]
            if s>i:
                break
            elif s==i and j==len(aa)-1:
                #print str(i)
                ret+=i
        i+=1
    
    return ret



if __name__ == "__main__":
    start_time=time.time()
    print(main())
    print("time: "+str(round(time.time()-start_time,3))+"s")

