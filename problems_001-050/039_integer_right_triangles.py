#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #39 Integer right triangles
#
# Author: Tuomo Huttu
# Date:   15.4.2018 (syntax updated 21.5.2020)

# Right answer: 840 (26s) (python2.7 8.0s)


import time


def main(x):
    ret=0
    lkm=0
    for p in range(9,x+1):
        lkm2=0
        for a in range(2,p):
            if int(3.414*a)>p: #1+1+sqrt(2)=3.4142
                break
            for b in range(a,p):
                c=p-a-b
                if b>=c:
                    break
                elif a**2+b**2==c**2:
                    lkm2+=1
        #print str(i)+" "+str(lkm2)
        if lkm2>lkm:
            lkm=lkm2
            ret=p
    
    return ret



if __name__ == "__main__":
    start_time=time.time()
    print(main(1000))
    print("time: "+str(round(time.time()-start_time,3))+"s")

