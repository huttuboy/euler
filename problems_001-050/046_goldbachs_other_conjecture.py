#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #46 Goldbach's other conjecture
#
# Author: Tuomo Huttu
# Date:   17.4.2018 (syntax updated 21.5.2020)

# Right answer: 5777 (0.11s) (python2.7 0.06s)


import time
from primes import is_prime

prime_list=[2,3]


def main():
    global prime_list
    
    a=3
    while True:
        a+=2
        if is_prime(a):
            prime_list.append(a)
        elif not is_gold(a):
            return a
    
    return


def is_gold(a):
    global prime_list
    
    for i in range(len(prime_list)):
        #b=(a-prime_list[i])/2
        b=(a-prime_list[i])//2
        if int(b**0.5)**2==b:
            return True
    return False



if __name__ == "__main__":
    start_time=time.time()
    print(main())
    print("time: "+str(round(time.time()-start_time,3))+"s")

