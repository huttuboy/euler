#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #19 Counting Sundays
#
# Author: Tuomo Huttu
# Date:   10.4.2018 (syntax updated 21.5.2020)

# Right answer: 171 (0.035s) (python2.7 0.03s)

# 1.31 2.28(29) 3.31 4.30 5.31 6.30 7.31 8.31 9.30 10.31 11.30 12.31


import time


def main():
    date=[1,1,1,1900]
    sundays=0
    while date[3]<2001:
        date=next_day(date)
        if date[0]==7 and date[1]==1 and date[3]>=1901 and date[3]<2001:
            sundays+=1
    
    return sundays


def next_day(date):
    wday=date[0]+1
    if wday>7:
        wday=1
    
    day=date[1]+1
    mon=date[2]
    year=date[3]
    
    if mon in [1,3,5,7,8,10,12] and day>31:
        day=1
        mon+=1
    elif mon in [4,6,9,11] and day>30:
        day=1
        mon+=1
    elif mon==2 and year%4!=0 and day>28:
        day=1
        mon+=1
    elif mon==2 and year%4==0 and day>28:
        if year%100==0 and year%400!=0 and day>28:
            day=1
            mon+=1
        elif day>29:
            day=1
            mon+=1
    
    if mon>12:
        mon=1
        year+=1
    
    return [wday,day,mon,year]


if __name__ == "__main__":
    start_time=time.time()
    print(main())
    print("time: "+str(round(time.time()-start_time,3))+"s")

