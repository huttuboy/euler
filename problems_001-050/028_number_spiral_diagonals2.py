#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #28 Number spiral diagonals
#
# Author: Tuomo Huttu
# Date:   9.1.2020 (syntax updated 21.5.2020)

# Right answer: 669171001 (0s)


import time


def main(x):
    diag=[1]
    last=1
    a=2
    while a<x:
        for i in range(4):
            last+=a
            diag.append(last)
        a+=2

    return sum(diag)



if __name__ == "__main__":
    start_time=time.time()
    #print str(main(5)) #101
    print(main(1001))
    print("time: "+str(round(time.time()-start_time,3))+"s")

