#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #40 Champernowne's constant
#
# Author: Tuomo Huttu
# Date:   15.4.2018 (syntax updated 21.5.2020)

# Right answer: 210 (0.051s)


import time


def main():
    a=""
    i=1
    while len(a)<1000000:
        a+=str(i)
        i+=1
    ret=1
    for i in range(7):
        #print a[10**i-1]
        ret*=int(a[10**i-1])
    
    return ret



if __name__ == "__main__":
    start_time=time.time()
    print(main())
    print("time: "+str(round(time.time()-start_time,3))+"s")

