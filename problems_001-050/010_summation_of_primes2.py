#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #10 Summation of primes
#
# Author: Tuomo Huttu
# Date:   6.1.2020 (syntax updated 21.5.2020)

# Right answer: 142913828922 (9.5s) (python2.7 6.4s)



import time
from primes import *


def main(x):
    pr=get_primelist(0,x-1)
    return sum(pr)



if __name__ == "__main__":
    start_time=time.time()
    print(main(2000000))
    print("time: "+str(round(time.time()-start_time,3))+"s")

