#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #24 Lexicographic permutations
#
# Author: Tuomo Huttu
# Date:   11.4.2018 (syntax updated 21.5.2020)

# Right answer: 2783915460 (0s)


import time
#from factorial import factorial
from math import factorial


def main(place): #place=0 is first
    #L=range(10)
    L=list(range(10))
    lex=lexico(L,place)
    
    ret=""
    for a in lex:
        ret+=str(a)
    
    return ret


def lexico(L,place):
    le=len(L)
    if le==1:
        return L
    fa=factorial(le-1)
    place2=place
    i=0
    while place2+1>fa:
        place2-=fa
        i+=1
    
    if place2==0:
        return L[i:i+1]+L[0:i]+L[(i+1):]
    else:
        return L[i:i+1]+lexico(L[0:i]+L[(i+1):],place2)


if __name__ == "__main__":
    start_time=time.time()
    print(main(999999))
    print("time: "+str(round(time.time()-start_time,3))+"s")

