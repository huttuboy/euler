#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #44 Pentagon numbers
#
# Author: Tuomo Huttu
# Date:   11.1.2020 (syntax updated 21.5.2020)

# Right answer: 5482660 (1.92s) (python2.7 1.25s)


import time
from sortedlist import *


def main():
    pent=[1]
    i=2
    while True:
        #new=i*(3*i-1)/2
        new=i*(3*i-1)//2
        for j in range(i-1):
            pent_j=pent[j]
            #if is_pentagonal(new+pent_j) and is_pentagonal(new-pent_j):
            if is_in_sortedlist(pent,new-pent_j) and is_pentagonal(new+pent_j):
                return new-pent_j
        pent.append(new)
        i+=1
    return


def is_pentagonal(x):
    #n=(1+(1+24*x)**0.5)/6
    #m=int(n)
    m=(1+(1+24*x)**0.5)//6
    if x==m*(3*m-1)/2:
        return True
    return False



if __name__ == "__main__":
    start_time=time.time()
    print(main())
    print("time: "+str(round(time.time()-start_time,3))+"s")

