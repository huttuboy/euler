#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #1 Multiples of 3 and 5
#
# Author: Tuomo Huttu
# Date:   9.4.2018

# Right answer: 233168



def main(x):
    s=0
    for i in range(1,x):
        if i%3==0 or i%5==0:
            s+=i
    return s


if __name__ == "__main__":
    print(str(main(1000)))

