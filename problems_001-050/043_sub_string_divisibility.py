#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #43 Sub-string divisibility
#
# Author: Tuomo Huttu
# Date:   16.4.2018 (syntax updated 21.5.2020)

# Right answer: 16695334890 (time: 11.827s)



import time
from itertools import permutations


def main():
    aa=list(permutations("0123456789"))
    pr=[2,3,5,7,11,13,17]
    ret=0
    for a in aa:
        b=combine(a)
        for i in range(7):
            if int(b[i+1:i+4])%pr[i]!=0:
                break
            elif i==6:
                print(b)
                ret+=int(b)
    
    return ret


def combine(L):
    ret=""
    for i in L:
        ret+=str(i)
    return ret


if __name__ == "__main__":
    start_time=time.time()
    print(main())
    print("time: "+str(round(time.time()-start_time,3))+"s")

