#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #35 Circular primes
#
# Author: Tuomo Huttu
# Date:   10.1.2020 (syntax updated 21.5.2020)

# Right answer: 55 (3.8s) (python2.7 2.7s)


import time
from primes import *


def main(x):
    pr=get_primelist(10,x)

    ret=4 #2,3,5,7
    for p in pr:
        if is_rotations_primes(p):
            ret+=1
            #print str(a)
    return ret


def is_rotations_primes(x):
    a=str(x)
    
    if "0" in a or "2" in a or "4" in a or "6" in a or "8" in a:
        return False
    
    for i in range(1,len(a)):
        if not is_prime(int(a[i:]+a[0:i])):
            return False
    
    return True



if __name__ == "__main__":
    start_time=time.time()
    print(main(1000000))
    print("time: "+str(round(time.time()-start_time,3))+"s")

