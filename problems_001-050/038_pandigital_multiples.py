#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #38 Pandigital multiples
#
# Author: Tuomo Huttu
# Date:   15.4.2018 (syntax updated 21.5.2020)

# Right answer: 932718654 (0.02s)


import time


def main():
    ret=0
    for i in range(1,10001):
        aa=str(i)
        for j in range(2,10):
            aa+=str(j*i)
            if len(aa)>9:
                break
            elif len(aa)==9 and is_pandigital(aa):
                if int(aa)>ret:
                    ret=int(aa)
                    #print aa
    
    return ret


def is_pandigital(x):
    a=str(x)
    if "0" in a:
        return False
    for i in range(1,10):
        if str(i) not in a:
            return False
    return True



if __name__ == "__main__":
    start_time=time.time()
    print(main())
    print("time: "+str(round(time.time()-start_time,3))+"s")

