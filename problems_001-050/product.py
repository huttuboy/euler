#!/usr/bin/env python
# -*- coding: utf-8 -*-



def product(L):
    ret=1
    for i in L:
        ret*=i
    return ret



if __name__ == "__main__":
    print(product([2, 2, 2, 3, 3, 5, 7]))

