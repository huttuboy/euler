#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #32 Pandigital products
#
# Author: Tuomo Huttu
# Date:   14.4.2018 (syntax updated 21.5.2020)

# Right answer: 45228 (1.3s)


import time
ret=[]


def main():
    global ret
    combs("","123456789")
    
    return sum(ret)


def combs(inp,left):
    if len(left)>1:
        for i in range(len(left)):
            combs(inp+left[i],left[0:i]+left[i+1:])
    else:
        pan_check(inp+left)
        return 0


def pan_check(x):
    if int(x[0])*int(x[1:5])==int(x[5:]) or int(x[0:2])*int(x[2:5])==int(x[5:]):
        global ret
        if int(x[5:]) not in ret:
            #print x[5:]
            ret.append(int(x[5:]))
    return 0



if __name__ == "__main__":
    start_time=time.time()
    print(main())
    print("time: "+str(round(time.time()-start_time,3))+"s")

