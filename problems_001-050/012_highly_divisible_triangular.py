#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #12 Highly divisible triangular number
#
# Author: Tuomo Huttu
# Date:   11.4.2018 (syntax updated 21.5.2020)

# Right answer: 76576500 (6.0s) (python2.7 4.1s)



import time
from powerset import powerset
from product import product

prime_list=[2,3]


def main(x):
    print("x="+str(x))
    a=1
    b=0
    while True:
        b+=a
        a+=1
        if div_count(b)>x:
            return b
    
    return 0



def div_count(x):
    pr=primes(x)
    a=[]
    a.append(1)
    ps=powerset(pr)
    for p in ps:
        aa=product(p)
        if aa not in a:
            a.append(aa)
    return len(a)


def primes(x):
    global prime_list
    
    last=prime_list[len(prime_list)-1]
    ret=[]
    x2=x
    i=0
    while True:
        if i>=len(prime_list):
            last=new_prime(last)
            prime_list.append(last)
        if x2%prime_list[i]==0:
            ret.append(prime_list[i])
            x2/=prime_list[i]
        else:
            i+=1
        if x2==1:
            break
    
    return ret

def new_prime(last):
    new=last
    while True:
        new+=2
        if is_prime(new):
            return new

def is_prime(x):
    if x>9:
        r=int(x**0.5)
        f=5
        while f<=r:
            if x%f==0 or x%(f+2)==0:
                return False
            f+=6
    elif x<=1:
        return False
    elif x<4:
        return True
    elif x%2==0 or x%3==0:
        return False
    return True


if __name__ == "__main__":
    start_time=time.time()
    print(main(500))
    print("time: "+str(round(time.time()-start_time,3))+"s")

