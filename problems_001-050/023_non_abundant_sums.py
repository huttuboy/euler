#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #23 Non-abundant sums
#
# Author: Tuomo Huttu
# Date:   11.4.2018 (syntax updated 21.5.2020)

# Right answer: 4179871 (36s) (python2.7 24s)



import time
from powerset import powerset
from product import product

prime_list=[2,3]


def main():
    abundants=[]
    for i in range(12,28123):
        if div_sum(i)>i:
            abundants.append(i)
    ab_lkm=len(abundants)
    #print str(ab_lkm)
    
    ret=0
    for i in range(1,28123):
        ok=True
        for j in range(ab_lkm):
            if abundants[j]>i:
                break
            elif is_value_in_list(i-abundants[j],abundants):
                ok=False
                break
        if ok:
            ret+=i
            #print str(i)
    
    return ret


def is_value_in_list(value,L):
    a=0
    b=len(L)
    while b-a>10:
        #c=(a+b)/2
        c=int((a+b)/2)
        if L[c]==value:
            return True
        elif L[c]>value:
            b=c+1
        elif L[c]<value:
            a=c
    for i in range(a,b):
        if L[i]==value:
            return True
    return False


def div_sum(x):
    pr=primes(x)
    a=[]
    a.append(1)
    ps=powerset(pr)
    for p in ps:
        aa=product(p)
        if aa not in a and aa!=x:
            a.append(aa)
    return sum(a)

def primes(x):
    global prime_list
    
    last=prime_list[len(prime_list)-1]
    ret=[]
    x2=x
    i=0
    while True:
        if i>=len(prime_list):
            last=new_prime(last)
            prime_list.append(last)
        if x2%prime_list[i]==0:
            ret.append(prime_list[i])
            x2/=prime_list[i]
        else:
            i+=1
        if x2==1:
            break
    
    return ret

def new_prime(last):
    new=last
    while True:
        new+=2
        if is_prime(new):
            return new

def is_prime(x):
    if x>9:
        r=int(x**0.5)
        f=5
        while f<=r:
            if x%f==0 or x%(f+2)==0:
                return False
            f+=6
    elif x<=1:
        return False
    elif x<4:
        return True
    elif x%2==0 or x%3==0:
        return False
    return True


if __name__ == "__main__":
    start_time=time.time()
    print(main())
    print("time: "+str(round(time.time()-start_time,3))+"s")

