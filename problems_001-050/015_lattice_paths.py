#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #15 Lattice paths
#
# Author: Tuomo Huttu
# Date:   10.4.2018 (syntax updated 21.5.2020)

# Right answer: 137846528820 (0s)



import time


def main(x):
    #return factorial(2*x)/(factorial(x))**2
    return factorial(2*x)//(factorial(x))**2



def factorial(n):
    if n==0:
        return 1
    else:
        return n*factorial(n-1)



if __name__ == "__main__":
    start_time=time.time()
    print(main(20))
    print("time: "+str(round(time.time()-start_time,3))+"s")

