#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #36 Double-base palindromes
#
# Author: Tuomo Huttu
# Date:   15.4.2018 (syntax updated 21.5.2020)

# Right answer: 872187 (0.47s)


import time


def main(x):
    ret=0
    for i in range(x):
        if is_palindrome(i) and is_palindrome(int_to_binary(i)):
            ret+=i
            #print str(i)+" "+int_to_binary(i)
    
    return ret


def is_palindrome(x):
    a=str(x)
    if a==a[::-1]:
        return True
    return False


def int_to_binary(x):
    return "{0:b}".format(x)



if __name__ == "__main__":
    start_time=time.time()
    print(main(1000000))
    print("time: "+str(round(time.time()-start_time,3))+"s")

