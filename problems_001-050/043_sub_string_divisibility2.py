#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #43 Sub-string divisibility
#
# Author: Tuomo Huttu
# Date:   11.1.2020 (syntax updated 21.5.2020)

# Right answer: 16695334890 (6.5s) (python2.7 5.6s)


import time
from itertools import permutations


def main():
    aa=list(permutations("0123456789"))
    pr=[2,3,5,7,11,13,17] #primes
    ret=0
    for a in aa:
        b="".join(a)
        for i in range(7):
            if int(b[i+1:i+4])%pr[i]!=0:
                break
            elif i==6:
                #print b
                ret+=int(b)
    
    return ret



if __name__ == "__main__":
    start_time=time.time()
    print(main())
    print("time: "+str(round(time.time()-start_time,3))+"s")

