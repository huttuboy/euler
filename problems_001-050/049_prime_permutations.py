#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #49 Prime permutations
#
# Author: Tuomo Huttu
# Date:   18.4.2018 (syntax updated 21.5.2020)

# Right answer: 296962999629 (0.08s)


import time
from itertools import permutations
from primes import is_prime
#from combine import combine
from powerset import powerset2


def main():
    ret=[]
    for i in range(1000,9999):
        if is_prime(i):
            L=list_to_primes(list(permutations(str(i))))
            if len(L)>=3:
                ar=find_arit_seq(L)
                if ar>0 and ar not in ret:
                    ret.append(ar)
    #print ret
    return ret[1]


def list_to_primes(L):
    ret=[]
    for a in L:
        #b=int(combine(a))
        b=int("".join(a))
        if b>1000 and is_prime(b) and b not in ret:
            ret.append(b)
    ret.sort()
    return ret


def find_arit_seq(L):
    L2=powerset2(L,3)
    for a in L2:
        if a[1]-a[0]==a[2]-a[1]:
            #return int(combine([str(a[0]),str(a[1]),str(a[2])]))
            return int("".join([str(a[0]),str(a[1]),str(a[2])]))
    return 0



if __name__ == "__main__":
    start_time=time.time()
    print(main())
    print("time: "+str(round(time.time()-start_time,3))+"s")

