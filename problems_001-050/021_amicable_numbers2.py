#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #21 Amicable numbers
#
# Author: Tuomo Huttu
# Date:   8.1.2020 (syntax updated 21.5.2020)

# Right answer: 31626 (1.3s) (python2.7 0.9s)


import time
from powerset import powerset
from product import product
from primes import *


def main(x):
    ret=0
    for i in range(1,x):
        a=div_sum(i)
        if a!=i and a<x and div_sum(a)==i:
            ret+=a
    
    return ret


def div_sum(x):
    pr=primes(x)
    a=[1]
    ps=powerset(pr)
    for p in ps:
        aa=product(p)
        if aa not in a and aa!=x:
            a.append(aa)
    return sum(a)



if __name__ == "__main__":
    start_time=time.time()
    print(main(10000))
    print("time: "+str(round(time.time()-start_time,3))+"s")

