#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #3 Largest prime factor
#
# Author: Tuomo Huttu
# Date:   6.1.2020 (syntax updated 21.5.2020)

# Right answer: 6857 (0.005s)



import time
from primes import *


def main(x):
    x2=x
    pr=primes(x)
    
    return pr[len(pr)-1]



if __name__ == "__main__":
    start_time=time.time()
    #print(main(13195)) #29
    print(main(600851475143))
    print("time: "+str(round(time.time()-start_time,3))+"s")

