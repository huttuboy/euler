#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #14 Longest Collatz sequence
#
# Author: Tuomo Huttu
# Date:   7.1.2020 (syntax updated 21.5.2020)

# Right answer: 837799 (1.6s) (python2.7 1.3s)



import time

coll_list=[0]*3000000


def main(x):
    global coll_list
    
    biggest=[0,0]

    i=1
    while i<x:
        i+=1
        c=coll_list[i]
        if c>0:
            if c>biggest[1]:
                biggest=[i,c]
            continue
        c=collatz(i)
        coll_list[i]=c
        if c>biggest[1]:
            biggest=[i,c]

    return biggest[0]


def collatz(a):
    global coll_list
    
    b=a
    if b%2==0:
        #b/=2
        b//=2
    else:
        b=3*b+1
    
    if b==1:
        if a<3000000:
            coll_list[a]=1
        return 1
    
    if b<3000000:
        c=coll_list[b]
        if c>0:
            return c+1
    
    c=collatz(b)
    if b<3000000:
        coll_list[b]=c+1

    return c+1



if __name__ == "__main__":
    start_time=time.time()
    print(main(1000000))
    print("time: "+str(round(time.time()-start_time,3))+"s")

