#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #7 10001st prime
#
# Author: Tuomo Huttu
# Date:   6.1.2020 (syntax updated 21.5.2020)

# Right answer: 104743 (0.164s)


import time
from primes import *


def main(x):
    pr=get_n_primes(x)
    return pr[len(pr)-1]



if __name__ == "__main__":
    start_time=time.time()
    #print(main(6)) #13
    print(main(10001))
    print("time: "+str(round(time.time()-start_time,3))+"s")

