#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #4 Largest palindrome product
#
# Author: Tuomo Huttu
# Date:   9.4.2018 (syntax updated 21.5.2020)

# Right answer: 906609 (0.287s)


import time


def main():
    biggest=0
    for i in range(100,1000):
        for j in range(i,1000):
            apu=i*j
            if apu==reverse_number(apu) and apu>biggest:
                biggest=apu
    
    return biggest


def reverse_number(x):
    return int(str(x)[::-1])



if __name__ == "__main__":
    start_time=time.time()
    print(main())
    print("time: "+str(round(time.time()-start_time,3))+"s")

