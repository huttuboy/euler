#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #41 Pandigital prime
#
# Author: Tuomo Huttu
# Date:   16.4.2018 (syntax updated 21.5.2020)

# Right answer: 7652413 (over 2min)



import time


def main():
    ret=0
    a=7650001
    while a<987654322:
        a+=2
        if is_pandigital_n(a) and is_prime(a): ## time: 3.124s a 11-7654322
        #if is_prime(a) and is_pandigital_n(a): ## time: 41.441s a 11-7654322
            ret=a
            print(a)
    return ret


def is_pandigital_n(x):
    a=str(x)
    if "0" in a:
        return False
    for i in range(1,len(a)+1):
        if str(i) not in a:
            return False
    return True


def is_prime(x):
    if x<=1:
        return False
    elif x<4:
        return True
    elif x%2==0 or x%3==0:
        return False
    elif x>9:
        r=int(x**0.5)
        f=5
        while f<=r:
            if x%f==0 or x%(f+2)==0:
                return False
            f+=6
    
    return True


if __name__ == "__main__":
    start_time=time.time()
    print(main())
    print("time: "+str(round(time.time()-start_time,3))+"s")

