#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #42 Coded triangle numbers
#
# Author: Tuomo Huttu
# Date:   11.1.2020 (syntax updated 21.5.2020)

# Right answer: 162 (0.006s)


import time

abc="ABCDEFGHIJKLMNOPQRSTUVWXYZ" #26kpl


def main():
    tri_num=[1]
    last=1
    a=2
    while last<364: ##pisin sana 14, 14*26=364
        new=last+a
        tri_num.append(new)
        last=new
        a+=1
    word_list=read_file("p042_words.txt")
    ret=0
    for w in word_list:
        if word_value(w) in tri_num:
            ret+=1
    
    return ret


def word_value(x):
    global abc
    
    ret=0
    for i in range(len(x)):
        ret+=abc.find(x[i])+1
    
    return ret


def read_file(file):
    ret=[]
    f = open(file, "r")
    for line in f:
        sanat_h=line.split(",")
        for s in sanat_h:
            a=s.replace('"',"")
            ret.append(a)
    f.close()
    return ret



if __name__ == "__main__":
    start_time=time.time()
    print(main())
    print("time: "+str(round(time.time()-start_time,3))+"s")

