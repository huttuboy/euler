#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #14 Longest Collatz sequence
#
# Author: Tuomo Huttu
# Date:   10.4.2018 (syntax updated 21.5.2020)

# Right answer: 837799 (55s) (python2.7 25.4s)



import time

def main(x):
    longest=0
    start=0
    for i in range(2,x):
        b=i
        a=1
        while True:
            a+=1
            if b%2==0:
                b/=2
            else:
                b=3*b+1
            if b==1:
                if a>longest:
                    longest=a
                    start=i
                break
    
    return start



if __name__ == "__main__":
    start_time=time.time()
    print(main(1000000))
    print("time: "+str(round(time.time()-start_time,3))+"s")

