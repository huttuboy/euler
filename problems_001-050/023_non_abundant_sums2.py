#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #23 Non-abundant sums
#
# Author: Tuomo Huttu
# Date:   9.1.2020 (syntax updated 21.5.2020)

# Right answer: 4179871 (6.9s) (python2.7 4.8s)


import time
from powerset import powerset
from product import product
from primes import *


def main():
    abundants=[]
    i=12
    while i<28124:
        if div_sum(i)>i:
            abundants.append(i)
        i+=1
    ab_lkm=len(abundants)
    
    #nums=range(28124)
    nums=list(range(28124))
    for i in range(ab_lkm):
        for j in range(i,ab_lkm):
            absum=abundants[i]+abundants[j]
            if absum>28123:
                break
            if nums[absum]>0:
                nums[absum]=0
    
    return sum(nums)


def div_sum(x):
    pr=primes(x)
    a=[]
    a.append(1)
    ps=powerset(pr)
    for p in ps:
        aa=product(p)
        if aa not in a and aa!=x:
            a.append(aa)
    return sum(a)



if __name__ == "__main__":
    start_time=time.time()
    print(main())
    print("time: "+str(round(time.time()-start_time,3))+"s")

