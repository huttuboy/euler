#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #2 Even Fibonacci numbers
#
# Author: Tuomo Huttu
# Date:   9.4.2018 (syntax updated 21.5.2020)

# Right answer: 4613732



def main():
    num1=1
    num2=1
    s=0
    while num2<4000000:
        num3=num1+num2
        num1=num2
        num2=num3
        if num2%2==0:
            s+=num2
    
    return s



if __name__ == "__main__":
    print(main())

