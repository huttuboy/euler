#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #28 Number spiral diagonals
#
# Author: Tuomo Huttu
# Date:   14.4.2018 (syntax updated 21.5.2020)

# Right answer: 669171001 (0.33s) (python2.7 0.27s)


import time


def main(x):
    grid=[]
    for i in range(x):
        apu=[]
        for j in range(x):
            apu.append(0)
        grid.append(apu)
    
    #i=x/2
    #j=x/2
    i=x//2
    j=x//2
    grid[i][j]=1
    
    z=2
    #for k in range(x/2):
    for k in range(x//2):
        j+=1
        grid[i][j]=z
        z+=1
        for k2 in range(2*(k+1)-1):
            i+=1
            grid[i][j]=z
            z+=1
        for k2 in range(2*(k+1)):
            j-=1
            grid[i][j]=z
            z+=1
        for k2 in range(2*(k+1)):
            i-=1
            grid[i][j]=z
            z+=1
        for k2 in range(2*(k+1)):
            j+=1
            grid[i][j]=z
            z+=1
    
    ret=0
    for k in range(x):
        ret+=grid[k][k]
        ret+=grid[x-1-k][k]
    
    return ret-1



if __name__ == "__main__":
    start_time=time.time()
    print(main(1001))
    print("time: "+str(round(time.time()-start_time,3))+"s")

