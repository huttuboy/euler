#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #29 Distinct powers
#
# Author: Tuomo Huttu
# Date:   9.1.2020 (syntax updated 21.5.2020)

# Right answer: 9183 (0.029s)


import time
from sortedlist import *


def main():
    L=[]
    for a in range(2,101):
        for b in range(2,101):
            aa=a**b
            if not is_in_sortedlist(L,aa):
                append_sortedlist(L,aa)
    
    return len(L)



if __name__ == "__main__":
    start_time=time.time()
    print(main())
    print("time: "+str(round(time.time()-start_time,3))+"s")

