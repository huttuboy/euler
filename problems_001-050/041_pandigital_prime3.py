#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #41 Pandigital prime
#
# Author: Tuomo Huttu
# Date:   11.1.2020 (syntax updated 21.5.2020)

# Right answer: 7652413 (0.42s)


import time
from itertools import permutations
from primes import *


def main():
    ret=0
    num="987654321"
    for i in range(10):
        aa=list(permutations(num[i:]))
        for a in aa:
            joined=int("".join(a))
            if is_prime(joined):
                ret=joined
                break
        if ret:
            break
    
    return ret



if __name__ == "__main__":
    start_time=time.time()
    print(main())
    print("time: "+str(round(time.time()-start_time,3))+"s")

