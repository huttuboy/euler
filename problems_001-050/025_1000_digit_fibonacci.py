#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #25 1000-digit Fibonacci number
#
# Author: Tuomo Huttu
# Date:   12.4.2018 (syntax updated 21.5.2020)

# Right answer: 4782 (0.06s)



import time


def main(x):
    f1=1
    f2=1
    i=2
    while len(str(f2))<x:
        #print str(i)
        new=f1+f2
        f1=f2
        f2=new
        i+=1
    
    return i



if __name__ == "__main__":
    start_time=time.time()
    print(main(1000))
    print("time: "+str(round(time.time()-start_time,3))+"s")

