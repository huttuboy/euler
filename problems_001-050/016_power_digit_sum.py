#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #16 Power digit sum
#
# Author: Tuomo Huttu
# Date:   10.4.2018 (syntax updated 21.5.2020)

# Right answer: 1366 (0s)



import time


def main(x):
    a=str(2**x)
    ret=0
    for i in range(len(a)):
        ret+=int(a[i])
    
    return ret



if __name__ == "__main__":
    start_time=time.time()
    print(main(1000))
    print("time: "+str(round(time.time()-start_time,3))+"s")

