#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #20 Factorial digit sum
#
# Author: Tuomo Huttu
# Date:   11.4.2018 (syntax updated 21.5.2020)

# Right answer: 648 (0s)


import time


def main(x):
    a=str(factorial(x))
    s=0
    for i in range(len(a)):
        s+=int(a[i])
    return s


def factorial(n):
    if n==0:
        return 1
    else:
        return n*factorial(n-1)


if __name__ == "__main__":
    start_time=time.time()
    print(main(100))
    print("time: "+str(round(time.time()-start_time,3))+"s")

