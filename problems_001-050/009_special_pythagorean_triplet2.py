#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #9 Special Pythagorean triplet
#
# Author: Tuomo Huttu
# Date:   6.1.2020 (syntax updated 21.5.2020)

# Right answer: 31875000 (0.028s)
# (200 375 425)


import time


def main(x):
    #a<b<c
    for a in range(1,int(x/3)):
        for b in range(a+1,x):
            if a+b+b>x:
                break
            c=x-a-b
            if a*a+b*b==c*c:
                print(str(a)+" "+str(b)+" "+str(c))
                return a*b*c



if __name__ == "__main__":
    start_time=time.time()
    print(main(1000))
    print("time: "+str(round(time.time()-start_time,3))+"s")

