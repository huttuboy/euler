#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #30 Digit fifth powers
#
# Author: Tuomo Huttu
# Date:   9.1.2020 (syntax updated 21.5.2020)

# Right answer: 443839 (1.33s) (python2.7 1.2s)



import time


def main():
    ret=0
    for i in range(2,354294): #6digits -> 6*9^5=354294
        a=str(i)
        s=0
        for j in range(len(a)):
            s+=int(a[j])**5
            if s>i:
                break
        if s==i:
            ret+=i
    
    return ret



if __name__ == "__main__":
    start_time=time.time()
    print(main())
    print("time: "+str(round(time.time()-start_time,3))+"s")

