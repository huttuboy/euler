#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #3 Largest prime factor
#
# Author: Tuomo Huttu
# Date:   6.1.2020 (syntax updated 21.5.2020)

# Right answer: 6857 (0.005s)



import time
from primes import *


def main(x):
    x2=x
    prime_list=get_primelist(0,7000)
    
    i=0
    while True:
        if x2%prime_list[i]==0:
            x2/=prime_list[i]
        if x2==1:
            return prime_list[i]
        i+=1



if __name__ == "__main__":
    start_time=time.time()
    #print str(main(13195)) #29
    print(main(600851475143))
    print("time: "+str(round(time.time()-start_time,3))+"s")

