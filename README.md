Project Euler
=============

Repository for my Project Euler solutions (and help files). [Project Euler](https://projecteuler.net/)

Author: Tuomo Huttu

Started 9.4.2018
