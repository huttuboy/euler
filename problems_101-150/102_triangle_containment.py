#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #102 Triangle containment
#
# Author: Tuomo Huttu
# Date:   22.5.2020

# Right answer: 228 (0.008s)


import time


def main():
    tri=read_file("p102_triangles.txt")
    ret=0
    for t in tri:
        #print(t)
        area=triangle_area(t[0],t[1],t[2],t[3],t[4],t[5])
        area1=triangle_area(0,0,t[2],t[3],t[4],t[5])
        area2=triangle_area(0,0,t[0],t[1],t[4],t[5])
        area3=triangle_area(0,0,t[0],t[1],t[2],t[3])
        if area1+area2+area3==area:
            ret+=1

    return ret


def triangle_area(x1,y1,x2,y2,x3,y3):
    return abs((x1*(y2-y3)+x2*(y3-y1)+x3*(y1-y2))/2.0)


def read_file(file):
    ret=[]
    f = open(file, "r")
    for line in f:
        line2=line.replace("\n","")
        points=line2.split(",")
        p2=[]
        for p in points:
            p2.append(int(p))
        ret.append(p2)
    f.close()
    return ret



if __name__ == "__main__":
    start_time=time.time()
    print(main())
    print("time: "+str(round(time.time()-start_time,3))+"s")

