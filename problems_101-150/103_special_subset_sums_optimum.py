#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #103 Special subset sums: optimum
#
# Author: Tuomo Huttu
# Date:   1.6.2020

# Right answer: 20313839404245 (0.062s)


import time
from itertools import combinations


def main():
    a=[11,18,19,20,22,25] #n=5
    a_sum=sum(a)
    
    pairs=[]
    for i in range(len(a)-1):
        for j in range(i,len(a)):
            s=a[i]+a[j]
            if s not in pairs:
                pairs.append(s)
    pairs.sort()
    #print(pairs)

    ret=[20]
    combs=list(combinations(pairs,6))
    min_sum=999
    min_set=""
    for c in combs:
        comb=ret+list(c)
        
        ok=True
        prev_max=0
        for n in range(2,7):
            set_sums=[]
            sets=list(combinations(comb,n))
            for s in sets:
                ssum=sum(s)
                if ssum not in set_sums and ssum>prev_max:
                    set_sums.append(ssum)
                else:
                    ok=False
                    break
            if not ok:
                break
            prev_max=max(set_sums)

        if ok and sum(comb)<min_sum:
            min_sum=sum(comb)
            min_set=str(comb)
    
    #print(min_sum)
    return min_set.replace("[","").replace("]","").replace(",","").replace(" ","")



if __name__ == "__main__":
    start_time=time.time()
    print(main())
    print("time: "+str(round(time.time()-start_time,3))+"s")

