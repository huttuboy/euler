#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Project Euler, projecteuler.net
# Problem: #105 Special subset sums: testing
#
# Author: Tuomo Huttu
# Date:   1.6.2020

# Right answer: 73702 (0.131s)


import time
from itertools import combinations


def main():
    sets=read_file("p105_sets.txt")

    ret=0
    for s in sets:
        ok=True
        prev_max=0
        for n in range(2,len(s)):
            set_sums=[]
            sets2=list(combinations(s,n))
            for s2 in sets2:
                ssum=sum(s2)
                if ssum not in set_sums and ssum>prev_max:
                    set_sums.append(ssum)
                else:
                    ok=False
                    break
            if not ok:
                break
            prev_max=max(set_sums)

        if ok:
            ret+=sum(s)
    
    return ret



def read_file(file):
    ret=[]
    f = open(file, "r")
    for line in f:
        line2=line.replace("\n","")
        points=line2.split(",")
        p2=[]
        for p in points:
            p2.append(int(p))
        p2.sort()
        ret.append(p2)
    f.close()
    return ret



if __name__ == "__main__":
    start_time=time.time()
    print(main())
    print("time: "+str(round(time.time()-start_time,3))+"s")

